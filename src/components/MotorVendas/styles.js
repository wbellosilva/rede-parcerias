import styled from 'styled-components';

export const Container = styled.div`
  section.backgroundPlanos {
    padding: 10px;

    background-position: bottom;
    background-repeat: no-repeat;
    background-size: cover;
    max-height: 650px;
    margin-bottom: 150px;
    @media (max-width: 1040px) {
      max-height: 100%;
      margin-bottom: 80px;
    }

    div.planos {
      margin: 0 auto;
      max-width: 1100px;
      padding-top: 50px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      h3 {
        max-width: 1100px;
        font-size: 30px;
        color: #fff;
        margin-bottom: 25px;
        text-align: center;
        @media (max-width: 900px) {
          font-size: 25px;
        }
      }

      ul {
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        li {
          margin: 10px;
        }
      }
    }
  }
`;
