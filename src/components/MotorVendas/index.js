import React from 'react';
import FormVendas from '../FormVendas/index';
import StepForm01 from '../FormVendas/StepForm01/index';
import StepForm02 from '../FormVendas/StepForm02/index';
import StepForm03 from '../FormVendas/StepForm03/index';
import Erro from '../FormVendas/Erro/index';
import Sucesso from '../FormVendas/Sucesso/index';
import { Container } from './styles';
import MenuCards from '../MenuCards/index';

function MotorVendas() {
  return (
    <Container>
      <MenuCards />
      <FormVendas>
        <StepForm01 />
        <StepForm02 />
        <StepForm03 />
        <Sucesso />
        <Erro />
      </FormVendas>
    </Container>
  );
}

export default MotorVendas;
