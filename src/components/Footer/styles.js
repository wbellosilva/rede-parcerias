import styled from 'styled-components';

export const Container = styled.footer`
  background: #0d243c;
  padding: 10px 10px 30px 10px;

  div.footer {
    margin: 0 auto;
    padding: 20px 0;
    max-width: 1100px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: #fff;
    @media (max-width: 670px) {
      flex-direction: column;
      justify-content: center;
      > a {
        margin-bottom: 15px;
      }
      div.alinhamento {
        display: none;
      }
    }

    a.infos {
      @media (max-width: 670px) {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 25px;
      }
      span {
        margin: 4px 0;
        display: block;
        max-width: 350px;
      }
    }
    div.infos {
      margin: 10px;
      @media (max-width: 670px) {
        display: flex;
        align-items: center;
        flex-direction: column;
        margin: 25px;
      }
      span {
        margin: 4px 0;
        display: block;
        max-width: 350px;
      }
    }
    div.redesSociais {
      margin: 10px;

      display: flex;
      align-items: center;
      a {
        display: flex;
        align-items: center;
        margin-left: 10px;
        transition: all 0.2s;
        &:hover {
          transform: translateX(5px);
        }
      }
    }
    .text-center {
      text-align: center;
    }
    .whatsapp-tel {
      display: flex;
      justify-content: center;
    }
    .ml-2 {
      margin-left: 10px;
    }
  }

  .guia {
    color: white;
    text-align: center;
    font-size: 22px;
    position: relative;

    a {
      color: white;
      text-decoration: none;

      @media(max-width: 950px) {
        font-size: 18px;
      }
    }

    img {
      max-width: 100px;
      position: absolute;
      top: -35px;
      left: 20%;

      @media(max-width: 1320px) {
        left: 18%;
      }

      @media(max-width: 1250px) {
        left: 15%;
      }

      @media(max-width: 1135px) {
        left: 11%;
      }

      @media(max-width: 1020px) {
        left: 8%;
      }

      @media(max-width: 950px) {
        left: 6%;
      }

      @media(max-width: 798px) {
        position: relative;
        left: 0;
        display: block;
        margin: auto;
      }
    }
  }
`;