import React from 'react';

import { Container } from './styles';

function Footer() {
  return (
    <Container>
      <div className="guia">
        <a href="https://coobmais.com/guia-pratico/guia-digital-2021.pdf" target="_blank" download="https://coobmais.com/guia-pratico/guia-digital-2021.pdf">
          <img src="Sections/Footer/guiapratico.png" alt="Guia Prático" />
          <span>
            Clique e veja o <b>Guia Prático do Viajante Coob+</b>
          </span>
        </a>
      </div>
      <div className="footer">
        <div className="infos">
          <span>Rua Theobaldo Fleck, nº 145, Vila Suzana</span>
          <span>Canela, Rio Grande do Sul - Brasil</span>
          <span>Coob+ © Copyright 2021</span>
        </div>
        <a className="infos" href="https://www.coobmais.com/" target="_blank">
          <img
            src="Sections/Footer/logoCoobmais2.png"
            alt="Logotipo Coob+"
          />
        </a>

        <div>
          <div className="redesSociais">
            <a href="https://www.facebook.com/coobmais" target="_blank">
              <img src="Sections/Footer/icorodape-face.png" alt="Facebook" />
            </a>
            <a href="https://www.instagram.com/coobmais/" target="_blank">
              <img src="Sections/Footer/icorodape-insta.png" alt="Instagram" />
            </a>
            <a
              href="https://www.youtube.com/channel/UC85XIUgu7cBSdtCwRpxhDkg"
              target="_blank"
            >
              <img src="Sections/Footer/icorodape-yt.png" alt="Youtube" />
            </a>
            <a
              href="https://www.linkedin.com/company/coobmais-viagens-e-turismo-ltda"
              target="_blank"
            >
              <img
                src="Sections/Footer/icorodape-linkedin.png"
                alt="Linkedin"
              />
            </a>
            <a
              href="https://open.spotify.com/user/ych9gz1nmnidx0o6ei6npq7md?si=dd90ad79b785459c"
              target="_blank"
            >
              <img
                src="Sections/Footer/icorodape-spotify.png"
                alt="Spotify"
              />
            </a>
            <a
              href="https://viajantego.com/blog/"
              target="_blank"
            >
              <img
                src="Sections/Footer/icorodape-blog.png"
                alt="Blog"
              />
            </a>
            <a
              href="https://www.tiktok.com/@coobmaisturismo?lang=pt-BR"
              target="_blank"
            >
              <img
                src="Sections/Footer/icorodape-tiktok.png"
                alt="Tik Tok"
              />
            </a>
          </div>
          <div className="text-center">
            <a
              className="whatsapp-tel"
              href="https://api.whatsapp.com/send?phone=54974008256"
              target="_blank"
            >
              <img src="Sections/Footer/icorodape-whats.png" alt="Whatsapp" />{' '}
              <span className="ml-2">(54) 97400-8256</span>
            </a>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default Footer;
