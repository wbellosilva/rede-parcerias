import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: flex-end;
  margin-top: -15px;
  justify-content: center;
  background: url('Sections/Faq/background.png');
  background-position: right center;
  background-repeat: no-repeat;
  span {
    font-size: 12px;
    font-weight: 700;
  }
  padding: 10px 10px 0 10px;
  section {
    max-width: 500px;
    width: 100%;
    padding-bottom: 60px;
    h1 {
      margin: 15px 0;
      font-size: 38px;
      font-weight: 900;
    }
  }
  img {
    width: 100%;
    max-height: 590px;
    max-width: 450px;
    margin-left: 50px;
    @media (max-width: 900px) {
      max-width: 300px;
    }
    @media (max-width: 700px) {
      display: none;
    }
  }
  .scroll {
    border-radius: 5px;
    box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.15);
    max-height: 600px;
    overflow-y: scroll;
    background: #fff;
    &::-webkit-scrollbar {
      width: 5px;
      background: #f4f4f4;
    }
    &::-webkit-scrollbar-thumb {
      background: #dad7d7;
    }
  }
  .MuiAccordionDetails-root {
    display: flex;
    flex-direction: column;
    font-size: 14px;
  }
`;
