import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Container } from './styles';

import { Perguntas } from '../../Data/Perguntas';

const Accordion = withStyles({
  root: {
    border: '',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);
const AccordionSummary = withStyles({
  root: {
    boxShadow: '1px 0px 5px  rgba(0,0,0,0.15)',
    margin: '2px',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

function Faq() {
  const [expanded, setExpanded] = React.useState('');

  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  return (
    <Container id="faq">
      <section>
        <h1>Perguntas frequentes</h1>
        <div className="scroll">
          {Perguntas.map(pergunta => (
            <Accordion
              square
              expanded={expanded === pergunta.id}
              onChange={handleChange(`${pergunta.id}`)}
              key={pergunta.pergunta}
            >
              <AccordionSummary
                aria-controls="panel1d-content"
                id={pergunta.id}
                expandIcon={
                  <ExpandMoreIcon
                    style={{
                      color: expanded === pergunta.id ? '#ff8257' : '#2e3d62',
                    }}
                  />
                }
                style={{
                  color: expanded === pergunta.id ? '#ff8257' : '#2e3d62',
                }}
              >
                <span>{pergunta.pergunta}</span>
              </AccordionSummary>

              <AccordionDetails>{pergunta.resposta}</AccordionDetails>
            </Accordion>
          ))}
        </div>
      </section>
      <img src="Sections/Faq/faq.png" alt="Foto da Atendente Clara" />
    </Container>
  );
}

export default Faq;
