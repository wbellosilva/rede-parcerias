import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container } from './styles';
import api from '../../services/api';
import { changeCard } from '../../store/modules/form/actions';

// eslint-disable-next-line react/prop-types
function CardPlano({ Plano }) {
  const dispatch = useDispatch();
  // eslint-disable-next-line react/prop-types
  const idCard = useSelector(state => state.form.idCard);

  // eslint-disable-next-line react/prop-types
  const { src, id, diarias, valorAdesao } = Plano;
  const [mensalidade, setMensalidade] = useState('');
  const [adesao, setAdesao] = useState('');

  function handleChange(idChecked) {
    dispatch(changeCard(idChecked));
  }

  // BUSCA MENSALIDADE
  useEffect(() => {
    async function loadValue() {
      const response = await api.get(
        '/Associe.asmx/consultaValoresAdesaoMensalidadeV2',
        {
          params: {
            dados: {
              in_tpplano: id,
              in_plFamilia: 0,
              in_diarias: diarias,
              in_mentpCodigo: '2',
            },
          },
        }
      );

      const respostaMensalidade = Number(response.data)
        .toFixed(2)
        .toString()
        .replace('.', ',');

      setMensalidade(respostaMensalidade);
    }
    loadValue();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // BUSCA ADESÃO
  useEffect(() => {
    async function loadValue() {
      const response = await api.get(
        '/Associe.asmx/consultaValoresAdesaoMensalidadeV2',
        {
          params: {
            dados: {
              in_tpplano: id,
              in_plFamilia: 0,
              in_diarias: diarias,
              in_mentpCodigo: '1',
            },
          },
        }
      );

      const respostaAdesao = response.data;

      setAdesao(respostaAdesao);
    }
    loadValue();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Container>
      <img src={src} alt="" />
      <span>A partir de</span>
      <strong>R$ {mensalidade}/mês</strong>
      <div className="divisao" />

      <span>Adesão 100% OFF</span>
      <div className="adesao">
        <p>de R$</p>
        <div className="valorDesconto">
          <div className="desconto" />
          <p> {adesao} </p>
        </div>
        <p>por</p>
      </div>
      <strong className="desc80"> R$ 00</strong>
      {/* <strong className="desc80"> R$ {valorAdesao}</strong> */}
      <div>
        {idCard === id ? (
          <button type="button" className="checked">
            Selecionado
          </button>
        ) : (
          <button
            type="button"
            className="selectPlano"
            onClick={() => handleChange(id)}
          >
            Assine já!
          </button>
        )}
      </div>
    </Container>
  );
}

export default CardPlano;
