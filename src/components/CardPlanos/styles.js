import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  box-shadow: 2px 2px 15px rgba(0, 0, 0, 0.1);
  border-radius: 15px;
  padding-bottom: 10px;
  background-color: #fff;
  font-size: 14px;
  strong {
    color: #f45c08;
    font-size: 16px;
  }

  .desc80 {
    color: #00afff;
  }
  img {
    border-radius: 15px 15px 0 0;
    width: 100%;
    margin-bottom: 10px;
  }
  div.divisao {
    width: 90%;
    height: 1px;
    background: #bababa;
    margin: 5px 0;
  }
  div.adesao {
    display: flex;
    margin-bottom: 5px;

    span {
      margin-right: 5px;
    }

    div.valorDesconto {
      position: relative;
      margin: 0 5px;
      div.desconto {
        transform: rotate(-20deg);
        top: 8px;
        right: -3px;
        position: absolute;
        width: 35px;
        height: 1px;
        background: red;
      }
    }
  }

  button.selectPlano {
    margin: 10px 0;
    border: none;
    border-radius: 8px;
    background: rgb(159, 193, 56);

    color: #fff;

    font-weight: bold;
    padding: 5px 20px;
  }

  button.checked {
    margin: 10px 0;

    border: none;
    cursor: not-allowed;
    color: #fff;
    border-radius: 8px;

    background: #f45c08;

    font-weight: bold;
    padding: 5px 20px;
  }
`;
