import styled, { keyframes, css } from 'styled-components';

export const Container = styled.div`
  margin-bottom: 50px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;

  > p {
    font-size: 13px;
    font-style: italic;
    text-align: center;
    line-height: 1.8;
    color: #898a8e;
    max-width: 600px;
    padding: 30px 0;
    border-bottom: 1px solid #e6e6e6;
  }
  form {
    padding: 10px;
    width: 700px;
    @media (max-width: 735px) {
      width: 500px;
    }
    @media (max-width: 565px) {
      width: 400px;
    }
    @media (max-width: 400px) {
      width: 300px;
    }

    div {
      label.labelAlinhamento {
        display: flex;
        align-items: center;
        margin-top: 15px;
        @media (max-width: 735px) {
          flex-direction: column;
          align-items: flex-start;
        }

        strong {
          display: block;
          width: 200px;
          margin-bottom: 10px;
        }
        input {
          background: #e6e6e6;
          color: #7f7f7f;
          width: 100%;
          border-radius: 4px;
          border: none;
          padding: 10px 15px;
        }
        select {
          font-family: 'Futura Bold', sans-serif;

          background: #e6e6e6;
          color: #7f7f7f;
          width: 100%;

          border-radius: 4px;
          border: none;
          padding: 10px;
        }
      }
    }

    span.error {
      font-size: 10px;
      margin-top: 5px;
      color: red;
    }
  }
`;

export const Alinhamento = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 15px;

  div {
    margin: 0;
    display: flex;
    @media (max-width: 400px) {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
    }
  }
  div.cpf {
    display: flex;
    align-items: center;
    justify-content: center;

    strong {
      display: block;
      width: 270px;

      @media (max-width: 735px) {
        width: 70px;
      }
      @media (max-width: 400px) {
        width: 100px;
      }
    }
    label {
      display: flex;
      align-items: center;

      input {
        width: 100%;
      }
    }
  }

  div.telefone {
    display: flex;
    align-items: center;
    @media (max-width: 400px) {
      margin-top: 15px;
      flex-direction: row;
    }

    label {
      display: flex;
      align-items: center;
      width: 100%;

      strong {
        margin: 0 10px;
        @media (max-width: 400px) {
          margin: 0 10px 0 0;
        }
      }
      input.ddd {
        width: 60px;
        margin-right: 10px;
      }
      input.fieldTelefone {
        width: 100%;
      }
    }
  }
  input {
    background: #e6e6e6;
    color: #7f7f7f;
    border-radius: 4px;
    border: none;
    padding: 10px 15px;
  }
`;

export const Footer = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: center;
  @media (max-width: 800px) {
    flex-direction: column;
    align-items: center;
  }

  div.infosFinais {
    margin-right: 10px;
  }
  div.buttons {
    margin-top: 15px;
    display: flex;
    align-items: center;

    button.voltar {
      display: flex;
      align-items: center;
      justify-content: center;

      background: #e6e6e6;
      padding: 7.5px;
      border-radius: 4px;
      border: none;
      font-size: 14px;
      color: #7f7f7f;
      margin-right: 15px;
      transition: all 0.2s;
      &:hover {
        background: #f1f0f0;
      }
    }
  }
`;

// rotação do icone
const rotate = keyframes`
  from {
    transform:rotate(0deg);
  }
  to{
    transform: rotate(360deg);
  }
`;

export const ButtonSubmit = styled.button.attrs(props => ({
  disabled: props.loading,
}))`
  width: 200px;
  padding: 10px;
  border-radius: 4px;
  font-weight: bold;
  border: none;
  background: #9fc138;
  color: #fff;
  &[disabled] {
    cursor: not-allowed;
    opacity: 0.6;
  }
  ${props =>
    props.loading &&
    css`
      svg {
        animation: ${rotate} 2s linear infinite;
      }
    `}
  &:hover {
    background: #72940c;
  }
`;
