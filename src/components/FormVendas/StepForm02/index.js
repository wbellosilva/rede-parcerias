/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

import { useDispatch, useSelector } from 'react-redux';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import RotateRightIcon from '@material-ui/icons/RotateRight';
import AdesaoMensalidade from '../AdesaoMensalidade/index';
import idCardString from '../../../utils/switchPlano';

import {
  nextPage,
  backPage,
  cadastro,
  addToken,
  modal,
  codigoVendedorIS,
} from '../../../store/modules/form/actions';
import api from '../../../services/api';

import { Container, Alinhamento, Footer, ButtonSubmit } from './styles';

export const schema = Yup.object().shape({
  nome: Yup.string()
    .min(4, 'Digite seu nome')
    .required('O nome é obrigatório!'),
  sobrenome: Yup.string()
    .min(4, 'Digite seu sobrenome')
    .required('O sobrenome é obrigatório!'),

  cpf: Yup.string()
    .min(11, 'Informe um CPF válido')
    .required('O CPF é obrigatório!'),
  ddd: Yup.number()
    .typeError('Digite apenas números')
    .min(2)
    .max(99, 'Apenas 2 digitos no DDD')
    .required('Informe o seu DDD!'),
  celular: Yup.number()
    .typeError('Digite apenas números')
    .min(9, 'Mínimo de 9 números')
    .required('Informe o seu número para contato!'),
  uf: Yup.string().required('Informe o seu Estado!'),
  email: Yup.string()
    .email('O e-mail não é válido!')
    .required('O e-mail é obrigatório!'),
});

function StepForm02() {
  const infoUser = useSelector(state => state.form.infosUser);
  const plano = useSelector(state => state.form.idCard);
  const diarias = useSelector(state => state.form.diarias);
  const familia = useSelector(state => state.form.familia);
  const token = useSelector(state => state.form.token);
  const codSiteVendedor = useSelector(state => state.form.codSiteVendedor);
  const params = useSelector(state => state.form.urlParams);

  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const recaptchaRef = React.useRef();
  const [reCaptcha, setReCaptcha] = React.useState(null);

  /** Geração do Token */
  useEffect(() => {
    async function getToken() {
      const response = await api.get('administracao.asmx/GeraToken');

      const tokenUser = response.data;
      dispatch(addToken(tokenUser));
    }

    getToken();
  }, []);
  function onChange(key) {
    setReCaptcha(key);
  }

  async function handleSubmit(values) {
    const conectado = navigator.onLine;
    if (!conectado) {
      dispatch(modal(true, 'Você está sem internet.'));
      return;
    }
    if (reCaptcha === null || reCaptcha === '') {
      dispatch(modal(true, 'Por favor, selecione o reCaptcha'));
      return;
    }
    const { nome, cpf, ddd, celular, uf, email, sobrenome, vendedor } = values;
    // Removendo todos os caracteres que não são números
    const cpfArrumado = cpf.replace(/\D/gim, '');
    const celularArrumado = celular.replace(/\D/gim, '');

    let in_vendCodigo;
    if (!(vendedor === '' || vendedor === null || vendedor === undefined)) {
      in_vendCodigo = vendedor.replace(/\D/gim, '');
      if (in_vendCodigo.length < 3) {
        in_vendCodigo = codSiteVendedor;
      }
    } else {
      in_vendCodigo = codSiteVendedor;
    }

    // Consulta e cadastro do usuário
    const dados = {
      in_assCPF_CNPJ: cpfArrumado,
      in_assTipo: 'F',
      in_assNome_RazaoSocial: `${nome} ${sobrenome}`,
      in_assNumCelular: celularArrumado,
      in_assEmailPessoal: email,
      in_UFRes: uf,
      in_assNumCelularDDD: ddd,
      in_token: token,
      in_plano: plano,
      in_diarias: diarias,
      in_familia: familia,
      in_vendCodigo,
      responseToken: reCaptcha,
    };

    try {
      setLoading(true);
      const response = await api.get('/Associe.asmx/consultaCdlRecaptchaV3', {
        params: {
          dados,
        },
      });

      const dadosMarketing = {
        in_identificador: 'Intenção de Compra',
        in_assCPF_CNPJ: cpfArrumado,
        in_assTipo: 'F',
        in_assNomeCompleto: nome,
        in_sobrenome: sobrenome,
        in_assNumCelular: celular,
        in_assEmailPessoal: email,
        in_UFRes: uf,
        in_assNumCelularDDD: ddd,
        in_plano: idCardString(plano),
        in_diarias: diarias,
        in_familia: familia,
        in_vendCodigo: params.$idvend ? params.$idvend : '',
        in_parceiro: 'Rede Parcerias',
        in_Cidade: '',
        in_source: params.utm_source ? params.utm_source : '',
        in_medium: params.utm_medium ? params.utm_medium : 'organico',
        in_campaign: params.utm_campaign
          ? params.utm_campaign
          : 'Site Rede Parcerias',
        in_metodoCompra: 'e-commerce',
        in_formaPagamento: '',
      };

      fetch('https://hooks.zapier.com/hooks/catch/8356258/og1bfjk/', {
        method: 'POST',
        body: JSON.stringify(dadosMarketing),
      });

      const { numPedido } = response.data.Table[0];
      const restricaoSPC = response.data.Table1[0].resultado;
      const dataUser = values;

      dispatch(cadastro(dataUser, numPedido, restricaoSPC));

      dispatch(codigoVendedorIS(in_vendCodigo));

      // proxima etapa Form
      setLoading(false);
      dispatch(nextPage());
    } catch (error) {
      if (typeof error.response.data === 'string') {
        dispatch(modal(true, error.response.data));
        setReCaptcha(null);
        setLoading(false);
        recaptchaRef.current.reset();
        return;
      }

      dispatch(modal(true, 'ReCaptcha inválido, por favor tente novamente.'));

      setLoading(false);
      recaptchaRef.current.reset();
    }
  }

  function handleBack() {
    // volta uma etapa Form
    dispatch(backPage());
  }
  return (
    <Container>
      <Formik
        initialValues={infoUser}
        validationSchema={schema}
        onSubmit={values => handleSubmit(values)}
      >
        {() => (
          <Form id="form">
            <div>
              <label className="labelAlinhamento">
                <strong>Nome:</strong>
                <Field type="text" name="nome" placeholder="Nome" />
              </label>
              <ErrorMessage className="error" name="nome" component="span" />
            </div>
            <div>
              <label className="labelAlinhamento">
                <strong>Sobrenome: </strong>
                <Field type="text" name="sobrenome" placeholder="Sobrenome" />
              </label>
              <ErrorMessage
                className="error"
                name="sobrenome"
                component="span"
              />
            </div>

            <Alinhamento>
              <div>
                <div className="cpf">
                  <label>
                    <strong>CPF:</strong>
                    <Field
                      type="text"
                      name="cpf"
                      placeholder="___.___.___-__"
                    />
                  </label>
                </div>

                <div className="telefone">
                  <label>
                    <strong>Telefone:</strong>
                    <Field
                      type="text"
                      name="ddd"
                      className="ddd"
                      placeholder="(__)"
                      maxLength="2"
                    />
                  </label>
                  <label>
                    <Field
                      type="text"
                      name="celular"
                      maxLength="9"
                      className="fieldTelefone"
                      placeholder="____-____"
                    />
                  </label>
                </div>
              </div>
              <ErrorMessage className="error" name="cpf" component="span" />
              <ErrorMessage className="error" name="ddd" component="span" />
              <ErrorMessage className="error" name="celular" component="span" />
            </Alinhamento>

            <div>
              <label className="labelAlinhamento">
                <strong>Estado:</strong>
                <Field name="uf" as="select">
                  <option value={null}>Selecione o Estado</option>
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="G0">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS">Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                </Field>
              </label>
              <ErrorMessage className="error" name="uf" component="span" />
            </div>

            <div>
              <label className="labelAlinhamento">
                <strong>E-mail:</strong>
                <Field type="email" name="email" placeholder="E-mail" />
              </label>
              <ErrorMessage className="error" name="email" component="span" />
            </div>
            <div>
              <label className="labelAlinhamento">
                <strong>
                  Cód. Consultor <br />
                  de Vendas
                </strong>
                <Field
                  type="text"
                  name="vendedor"
                  placeholder="Campo Opcional"
                />
              </label>
            </div>
          </Form>
        )}
      </Formik>
      <ReCAPTCHA
        className="recaptcha"
        ref={recaptchaRef}
        sitekey="6Ld0SkseAAAAAGsTCj1pOAZzI6qNNk_mL_zBJIHo"
        onChange={onChange}
      />
      <p>
        Você receberá suas diárias para que possa realizar a reserva da sua
        hospedagem. Reservas imediatas. Observar hotéis com exigência de 30 dias
        de antecedência para check-in.
      </p>
      <Footer>
        <div className="infosFinais">
          <AdesaoMensalidade />
        </div>
        <div className="buttons">
          <button className="voltar" type="button" onClick={() => handleBack()}>
            <ChevronLeftIcon />
          </button>

          <ButtonSubmit
            className="submit"
            type="submit"
            form="form"
            loading={loading}
          >
            {!loading ? 'Próximo' : <RotateRightIcon />}
          </ButtonSubmit>
        </div>
      </Footer>
    </Container>
  );
}

export default StepForm02;
