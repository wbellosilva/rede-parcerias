import styled from 'styled-components';

export const Container = styled.div`
  div.alinhamento {
    display: flex;
    align-items: center;
    @media (max-width: 800px) {
      flex-direction: column;
      margin: 10px 0;
    }
  }
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  button {
    margin: 15px 0;
    width: 200px;
    padding: 10px;
    border-radius: 4px;
    font-weight: bold;
    border: none;
    background: #9fc138;
    color: #fff;
    &:hover {
      background: #72940c;
    }
  }
  strong.atencao {
    background: #f45c08;
    padding: 4px 15px;
    margin: 20px 0 10px 0;
    color: #fff;
  }
`;

export const PlanoSelecionado = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-right: 1px solid #0d243c;
  margin-right: 15px;
  padding: 15px;
  @media (max-width: 800px) {
    border: none;
  }
  img {
    margin-top: 10px;
  }
`;
export const Infos = styled.div`
  @media (max-width: 800px) {
    display: flex;
    align-items: center;
    flex-direction: column;
    margin: 10px 0;
  }
  strong {
    margin: 15px 15px 5px 0;
    width: 300px;
  }

  div.quantidadeDePessoas {
    display: flex;
    align-items: center;
    @media (max-width: 800px) {
      flex-direction: column;
      align-items: flex-start;
      margin: 10px 0;
    }
    strong {
      margin: 10px 15px 10px 0;
    }
    div {
      display: flex;
      align-items: center;

      label {
        font-weight: bold;
        &:first-child {
          margin-right: 45px;
        }
        input {
          margin-right: 10px;
        }
      }
      span {
        margin-left: 15px;
        font-size: 12px;
        color: red;
      }
    }
  }
  div.quantidadeDeDias {
    display: flex;
    align-items: center;
    @media (max-width: 800px) {
      flex-direction: column;
      align-items: flex-start;
      margin-bottom: 10px;
      strong {
        margin-bottom: 10px;
      }
    }
    select {
      font-family: 'Futura Bold', sans-serif;
      font-weight: bold;
      background: #fff;
      border: 1px solid #7f7f7f;
      max-width: 250px;
      width: 100%;
      border-radius: 4px;
      padding: 10px 15px;
    }
    span {
      margin-left: 15px;
      font-size: 12px;
      color: red;
    }
  }
  div.planoGo {
    align-items: center;
    display: flex;
    @media (max-width: 800px) {
      flex-direction: column;
      align-items: flex-start;
      label {
        margin: 5px 0;
      }
    }
    label {
      font-weight: bold;

      margin-right: 10px;

      input {
        margin-right: 10px;
      }
    }
  }
`;
