/* eslint-disable import/no-dynamic-require */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import api from '../../../services/api';
import {
  nextPage,
  changeDiarias,
  changeFamilia,
  changeMensalidade,
  setAdesao,
  setAdesaoComDesconto,
} from '../../../store/modules/form/actions';
import AdesaoMensalidade from '../AdesaoMensalidade/index';

import { Container, Infos, PlanoSelecionado } from './styles';

function StepForm01() {
  const dispatch = useDispatch();

  // States do storie Redux
  const idCard = useSelector(state => state.form.idCard);
  const diariasa = useSelector(state => state.form.diarias);
  const familia = useSelector(state => state.form.familia);
  const codSiteVendedor = useSelector(state => state.form.codSiteVendedor);

  /** API DE CONSULTA DE MENSALIDADE */
  useEffect(() => {
    async function loadValue() {
      const response = await api.get(
        '/Associe.asmx/consultaValoresAdesaoMensalidadeV2',
        {
          params: {
            dados: {
              in_tpplano: idCard,
              in_plFamilia: familia,
              in_diarias: diariasa,
              in_mentpCodigo: '2',
            },
          },
        }
      );

      const mensalidade = response.data;
      dispatch(changeMensalidade(mensalidade));
    }

    loadValue();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [diariasa, familia, idCard]);
  /** Busca valor adesão com desconto */
  useEffect(() => {
    async function buscaAdesao() {
      const dados = {
        in_tpplano: idCard,
        in_plFamilia: familia,
        in_diarias: diariasa,
        in_codigoParceria: codSiteVendedor,

        in_mentpCodigo: '1',
      };

      const response = await api.get(
        '/Associe.asmx/consultaValoresAdesaoMensalidade',
        {
          params: {
            dados,
          },
        }
      );

      const resultAdesao = response.data;

      dispatch(setAdesaoComDesconto(resultAdesao));
    }

    // verifica se o plano é Go - Se for go não possui adesão
    if (idCard === '34' || idCard === '35') {
      dispatch(setAdesao(0.0));

      return;
    }
    buscaAdesao();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [diariasa, familia, idCard]);

  /** Busca valor adesão do planos */
  useEffect(() => {
    async function buscaAdesao() {
      const dados = {
        in_tpplano: idCard,
        in_plFamilia: familia,
        in_diarias: diariasa,
        in_mentpCodigo: '1',
      };
      const response = await api.get(
        '/Associe.asmx/consultaValoresAdesaoMensalidade',
        {
          params: {
            dados,
          },
        }
      );

      const resultAdesao = response.data;

      dispatch(setAdesao(resultAdesao));
    }

    // verifica se o plano é Go - Se for go não possui adesão
    if (idCard === '34' || idCard === '35') {
      dispatch(setAdesao(0));
      return;
    }
    buscaAdesao();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [diariasa, familia, idCard]);

  function handleClickPessoas({ target }) {
    const { value } = target;
    dispatch(changeFamilia(value));
  }

  function handleClickDiarias({ target }) {
    const { value } = target;
    dispatch(changeDiarias(value));
  }

  function handleSubmit() {
    dispatch(nextPage());
  }

  return (
    <Container>
      <div className="alinhamento">
        <PlanoSelecionado>
          <span>Você selecionou</span>
          <img src={`Sections/Planos/iconsPlano/${idCard}.png`} alt="" />
        </PlanoSelecionado>
        <div>
          <form id="form">
            <Infos>
              <div className="quantidadeDePessoas">
                <strong className="strongAlin">Quantidade de pessoas:</strong>
                <div>
                  <div>
                    <label>
                      <input
                        type="radio"
                        name="pessoas"
                        value="0"
                        checked={familia === '0'}
                        onClick={handleClickPessoas}
                      />
                      Até 02 pessoas
                    </label>

                    <label>
                      <input
                        type="radio"
                        name="pessoas"
                        checked={familia === '1'}
                        value="1"
                        onClick={handleClickPessoas}
                      />
                      Até 03 pessoas
                    </label>
                  </div>
                </div>
              </div>

              <div className="quantidadeDeDias">
                <strong className="strongAlin">
                  Quantidade de diárias desejadas:
                </strong>

                <select
                  name="diarias"
                  onChange={handleClickDiarias}
                  value={diariasa}
                >
                  <option value="7">07 diárias</option>
                  <option value="9">09 diárias</option>
                  <option value="11">11 diárias</option>
                  <option value="13">13 diárias</option>
                  <option value="15">15 diárias</option>
                  <option value="17">17 diárias</option>
                  <option value="19">19 diárias</option>
                  <option value="21">21 diárias</option>
                  <option value="23">23 diárias</option>
                  <option value="25">25 diárias</option>
                  <option value="27">27 diárias</option>
                  <option value="29">29 diárias</option>
                  <option value="31">31 diárias</option>
                  <option value="33">33 diárias</option>
                  <option value="35">35 diárias</option>
                  <option value="37">37 diárias</option>
                  <option value="39">39 diárias</option>
                  <option value="41">41 diárias</option>
                  <option value="43">43 diárias</option>
                  <option value="45">45 diárias</option>
                  <option value="47">47 diárias</option>
                  <option value="49">49 diárias</option>
                </select>
              </div>
            </Infos>
          </form>
          <AdesaoMensalidade />
        </div>
      </div>
      <strong className="atencao">
        Atenção: O valor da mensalidade é debitado no seu cartão de crédito mês
        a mês, sem comprometer o limite de compras.
      </strong>
      <button type="button" onClick={handleSubmit}>
        Próximo
      </button>
    </Container>
  );
}

export default StepForm01;
