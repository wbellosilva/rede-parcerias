import styled from 'styled-components';

export const Container = styled.div`
  max-width: 300px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;

  div {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background: #c70039;
    margin-bottom: 15px;
    span {
      color: #fff;
      font-weight: bold;
      transform: rotate(90deg);
    }
  }
  p {
    margin: 15px 0;
  }
  button {
    padding: 15px;
    background: #c70039;
    font-weight: bold;
    border: none;
    border-radius: 3px;
    color: #fff;
    &:hover {
      background: #943126;
    }
  }
`;
