import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { Container } from './styles';

import { inicioForm } from '../../../store/modules/form/actions';

function Erro() {
  const dispatch = useDispatch();
  const response = useSelector(state => state.form.responseRequest)
    .replace('<br>', ' ')
    .replace('<br>', ' ');

  function setp01() {
    dispatch(inicioForm(0));
    window.location.reload();
  }
  return (
    <Container>
      <div>
        <span>:(</span>
      </div>
      <strong>Algo deu errado :(</strong>
      <p>{response}</p>
      <button type="button" onClick={() => setp01()}>
        Voltar para o início
      </button>
    </Container>
  );
}

export default Erro;
