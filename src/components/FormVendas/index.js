/* eslint-disable react/prop-types */
import React from 'react';
import { useSelector } from 'react-redux';

import { Container } from './styles';

import Header from './Header/index';

function FormVendas({ children }) {
  const etapa = useSelector(state => state.form.etapa);

  const childrenArray = React.Children.toArray(children);
  const currentChild = childrenArray[etapa];

  return (
    <Container>
      <Header />
      {currentChild}
    </Container>
  );
}

export default FormVendas;
