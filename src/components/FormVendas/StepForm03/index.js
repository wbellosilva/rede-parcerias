/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */

import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import RotateRightIcon from '@material-ui/icons/RotateRight';
import ReCAPTCHA from 'react-google-recaptcha';
import api from '../../../services/api';

import CartaoDeCredito from './CartaoDeCredito/index';
import AdesaoMensalidade from '../AdesaoMensalidade/index';

import {
  backPage,
  isPan,
  responseRequest,
  erroValidationCartao,
  modal,
} from '../../../store/modules/form/actions';

import {
  Container,
  NomeCartao,
  Validade,
  CodVerificador,
  Cartao,
  Contrato,
  Footer,
  MetodoDePagamento,
  ButtonSubmit,
  PrimeiraMensalidade,
} from './styles';

export const schema = Yup.object().shape({
  nomeCartao: Yup.string().required('Digite o nome que está no cartão!'),
  mes: Yup.string().required('Informe o mês de vencimento do cartão!'),
  ano: Yup.string().required('Informe o ano de vencimento do cartão!'),
  cvv: Yup.string().required('Informe o codigo de segurança do cartão!'),
  numeroCartao: Yup.string().required('Informe o número do cartão!'),
  primeiraMensalidade: Yup.string().required(
    'Informe a data para pagamento mensal.'
  ),
  contrato: Yup.boolean().required('Você precisa aceitar o contrato!'),
});

function StepForm03() {
  const dispatch = useDispatch();
  const recaptchaRef = React.useRef();
  /** Dados do Redux */
  const codSiteVendedor = useSelector(state => state.form.codSiteVendedor);

  const inicioCartao = useSelector(state => state.form.isPan);
  const erroCartao = useSelector(state => state.form.erroCartao);
  const restricaoSPC = useSelector(state => state.form.restricaoSPC);

  const token = useSelector(state => state.form.token);
  const codigoVendedorIS = useSelector(state => state.form.codigoVendedorIS);
  const idCard = useSelector(state => state.form.idCard);
  const bandeiraCartao = useSelector(state => state.form.idCartaoCredito);
  const diarias = useSelector(state => state.form.diarias);
  const familia = useSelector(state => state.form.familia);
  const numPedido = useSelector(state => state.form.numPedido);
  const email = useSelector(state => state.form.infosUser.email);
  const totalMensalidade = Number(
    useSelector(state => state.form.totalMensalidade)
  )
    .toFixed(2)
    .toString()
    .replace('.', ',');
  const adesao = useSelector(state => state.form.adesao);

  function callJivo() {
    return window.jivo_api.open();
  }

  const [pagamentoCartao, setPagamentoCartao] = useState(true);
  const [loading, setLoading] = useState(false);

  function onBlurCartao(ev) {
    const { value } = ev.target;
    if (value.length > 4) {
      const numero = value.toString().substr(0, 6);
      dispatch(isPan(numero));
    }
  }

  function handleBack() {
    dispatch(backPage());
  }

  async function handleSubmit({
    cvv,
    mes,
    ano,
    numeroCartao,
    nomeCartao,
    primeiraMensalidade,
    contrato,
  }) {
    if (!contrato) {
      dispatch(modal(true, 'Você precisa acetar os termos do contrato!'));
      return;
    }
    const numeroCartaoArrumado = numeroCartao.replace(/\D/gim, '');
    if (bandeiraCartao === null) {
      dispatch(erroValidationCartao());
      dispatch(modal(true, 'Selecione a bandeira do seu cartão!'));
      return;
    }
    const conectado = navigator.onLine;
    if (!conectado) {
      dispatch(modal(true, 'Você está sem internet.'));
      return;
    }
    const recaptcha = await recaptchaRef.current.executeAsync();

    const dados = {
      in_token: token,
      in_plano: idCard,
      in_diarias: diarias,
      in_familia: familia,
      adiantamento: '0',
      numeroParcelas: '01',
      pedido: numPedido,
      numeroCartao: numeroCartaoArrumado,
      cvc: cvv,
      mes,
      ano,
      nomeCompleto: nomeCartao,
      mensalidade: totalMensalidade,
      primeiraCob: '01',
      mensMetPag: '3',
      diaDebito: primeiraMensalidade,
      in_vendCodigo: codigoVendedorIS,
      restricaoSPC,
      in_mentpCodigo: adesao,
      menCartaoBandeira: bandeiraCartao,
      in_codigoParceria: codSiteVendedor,
      responseToken: recaptcha,
    };

    try {
      setLoading(true);
      const finalizaCompra = await api.get(
        '/Associe.asmx/cadastroNovoAssociadoV5',
        {
          params: {
            dados,
          },
        }
      );
      const sucesso = finalizaCompra.data.TableFinal[0].mensagem;
      const dadosMarketing = {
        in_identificador: 'Compra Realizada',
        in_EmailPessoal: email,
        in_formaPagamento: 'Cartão de crédito',
      };

      fetch('https://hooks.zapier.com/hooks/catch/8356258/oe1k8z2/', {
        method: 'POST',
        body: JSON.stringify(dadosMarketing),
      });

      setLoading(false);

      dispatch(responseRequest(sucesso, 3));
    } catch (err) {
      recaptchaRef.current.reset();
      dispatch(modal(true, err.response.data));
      setLoading(false);
    }
  }

  return (
    <Container pagamentoCartao={pagamentoCartao}>
      <MetodoDePagamento>
        <label>
          <input
            type="radio"
            name="whatsapp"
            defaultChecked
            onClick={() => setPagamentoCartao(true)}
          />
          Efetuar pagamento com cartão
        </label>
      </MetodoDePagamento>

      <Formik
        initialValues={{
          nomeCartao: '',
          mes: '',
          ano: '',
          cvv: '',
          numeroCartao: '',
          contrato: '',
          primeiraMensalidade: '',
        }}
        validationSchema={schema}
        onSubmit={values => handleSubmit(values)}
      >
        {({ setFieldValue, values }) => (
          <Form id="form">
            <CartaoDeCredito />
            {erroCartao ? (
              <span className="error">Selecione a bandeira do seu cartão!</span>
            ) : null}

            <NomeCartao>
              <label>
                <strong>Nome do cartão:</strong>
                <Field type="text" name="nomeCartao" />
              </label>
              <ErrorMessage
                className="error"
                name="nomeCartao"
                component="span"
              />
            </NomeCartao>

            <Cartao pan={inicioCartao}>
              <label htmlFor="cartao">
                <strong>Número do Cartão de crédito</strong>
                <Field
                  type="text"
                  name="numeroCartao"
                  onBlur={ev => onBlurCartao(ev, setFieldValue, values)}
                />
              </label>
              <ErrorMessage
                className="error"
                name="numeroCartao"
                component="span"
              />
            </Cartao>

            <Validade>
              <div className="validade">
                <label>
                  <strong>Validade:</strong>
                  <Field name="mes" as="select">
                    <option value={null}>Mês</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </Field>
                </label>
                <ErrorMessage className="error" name="mes" component="span" />
              </div>

              <div className="validadeAno">
                <label>
                  <strong>/</strong>
                  <Field name="ano" as="select">
                    <option value="none">Ano</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                    <option value="2030">2030</option>
                    <option value="2031">2031</option>
                    <option value="2032">2032</option>
                    <option value="2033">2033</option>
                    <option value="2034">2034</option>
                    <option value="2035">2035</option>
                    <option value="2036">2036</option>
                    <option value="2037">2037</option>
                  </Field>
                </label>
                <ErrorMessage className="error" name="ano" component="span" />
              </div>
            </Validade>

            <CodVerificador>
              <label>
                <strong>Cód. Verificador:</strong>
                <Field type="text" name="cvv" />
              </label>
              <ErrorMessage className="error" name="cvv" component="span" />
            </CodVerificador>

            <PrimeiraMensalidade>
              <div className="primeiraMensalidade">
                <label>
                  <strong>Dia de transmissão da próxima mensalidade.</strong>
                  <Field name="primeiraMensalidade" as="select">
                    <option value="none">Dia</option>
                    <option value="05">05</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                  </Field>
                </label>
                <p>
                  Sugerimos selecionar um dia igual ao vencimento da sua fatura
                  ou posterior. Fique atento com a data escolhida, pois ela pode
                  gerar a cobrança de duas mensalidades na mesma fatura.
                </p>
              </div>
              <ErrorMessage
                className="error"
                name="primeiraMensalidade"
                component="span"
              />
            </PrimeiraMensalidade>
            <Contrato>
              <label>
                <Field type="checkbox" name="contrato" />
                <p>
                  Aceito as condições do
                  <a href="Contrato/CONTRATO_LEGAL_UNIFICADO.pdf" download="contrato_coobmais">
                    Contrato de Prestação de Serviço da Coob+
                  </a>
                  e a 
                  <a href="https://www.coobmais.com.br/politica-privacidade.html" target="_blank" rel="nofollow">
                    Política de Privacidade
                  </a>
                </p>
              </label>
              <ErrorMessage
                className="error"
                name="contrato"
                component="span"
              />
            </Contrato>
          </Form>
        )}
      </Formik>

      <MetodoDePagamento pagamentoCartao={pagamentoCartao}>
        <label>
          <input
            type="radio"
            name="whatsapp"
            onClick={() => setPagamentoCartao(false)}
          />
          Outra forma de pagamento.
        </label>
        <a href="javascript:void()" onClick={() => callJivo()}>
          Fale com um dos nossos consultores
        </a>
      </MetodoDePagamento>
      <p>
        Você receberá suas diárias para que possa realizar a reserva da sua
        hospedagem. Reservas imediatas. Observar hotéis com exigência de 30 dias
        de antecedência para check-in.
      </p>
      <Footer>
        <div className="infosFinais">
          <AdesaoMensalidade />

          <p>As Mensalidades poderão ser reajustadas, conforme previsão.</p>
        </div>
        <div className="buttons">
          <button className="voltar" type="button" onClick={() => handleBack()}>
            <ChevronLeftIcon />
          </button>

          <ButtonSubmit
            className="submit"
            type="submit"
            form="form"
            loading={loading}
          >
            {!loading ? 'Finalizar Compra' : <RotateRightIcon />}
          </ButtonSubmit>
        </div>
      </Footer>
      <ReCAPTCHA
        className="recaptcha"
        ref={recaptchaRef}
        sitekey="6Lf8H4IeAAAAACXNWTGb-JiCjnxPiyqPsHzQcxzS"
        size="invisible"
      />
    </Container>
  );
}

export default StepForm03;
