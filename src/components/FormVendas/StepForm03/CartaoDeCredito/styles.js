import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  margin-top: 25px;
  @media (max-width: 470px) {
    margin: 10px 0;
    flex-direction: column;
  }
  strong {
    display: block;
    width: 150px;
    margin: 0 18px 10px 0;
  }
  ul {
    display: flex;
    li {
      margin-right: 5px;

      button {
        background: none;
        border: 1px solid transparent;

        border-radius: 2px;
        display: flex;
        align-items: center;
        justify-content: center;
        &#visa {
          border: ${props =>
    props.idCartao === 1
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#crediCard {
          border: ${props =>
    props.idCartao === 2
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#master {
          border: ${props =>
    props.idCartao === 3
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#diners {
          border: ${props =>
    props.idCartao === 4
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#hipercard {
          border: ${props =>
    props.idCartao === 7
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#discover {
          border: ${props =>
    props.idCartao === 9
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#aura {
          border: ${props =>
    props.idCartao === 10
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#elo {
          border: ${props =>
    props.idCartao === 8
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }
        &#sicred {
          border: ${props =>
    props.idCartao === 11
      ? '2px solid #24ca01'
      : '1px solid transparent'};
          &:hover {
            border: 2px solid #24ca01;
          }
        }

        img {
          height: 20px;
          @media (max-width: 355px) {
            height: 18px;
          }
        }
      }
    }
  }
`;
