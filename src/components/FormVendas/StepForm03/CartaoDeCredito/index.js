import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { Container } from './styles';

import { changeCartaoCredito } from '../../../../store/modules/form/actions';

// imagens Cartões
function CartaoDeCredito() {
  const dispatch = useDispatch();
  const idCartao = useSelector(state => state.form.idCartaoCredito);

  function handleChangeCartao(id) {
    dispatch(changeCartaoCredito(id));
  }

  return (
    <Container idCartao={idCartao}>
      <strong>Escolha o seu cartão:</strong>
      <ul>
        <li>
          <button id="visa" type="button" onClick={() => handleChangeCartao(1)}>
            <img src="cartoes/visa.png" alt="Bandeira Visa" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="crediCard"
            onClick={() => handleChangeCartao(2)}
          >
            <img src="cartoes/credicard.png" alt="Bandeira Credicard" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="master"
            onClick={() => handleChangeCartao(3)}
          >
            <img src="cartoes/master.png" alt="Bandeira Master" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="diners"
            onClick={() => handleChangeCartao(4)}
          >
            <img src="cartoes/diners.png" alt="Bandeira Diners" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="hipercard"
            onClick={() => handleChangeCartao(7)}
          >
            <img src="cartoes/hipercard.png" alt="Bandeira Hipercard" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="discover"
            onClick={() => handleChangeCartao(9)}
          >
            <img src="cartoes/discover.png" alt="Bandeira Discover" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="aura"
            onClick={() => handleChangeCartao(10)}
          >
            <img src="cartoes/aura.png" alt="Bandeira Aura" />
          </button>
        </li>
        <li>
          <button type="button" id="elo" onClick={() => handleChangeCartao(8)}>
            <img src="cartoes/elo.jpg" alt="Bandeira Elo" />
          </button>
        </li>
        <li>
          <button
            type="button"
            id="sicred"
            onClick={() => handleChangeCartao(11)}
          >
            <img src="cartoes/sicredi.png" alt="Bandeira Sicred" />
          </button>
        </li>
      </ul>
    </Container>
  );
}

export default CartaoDeCredito;
