import styled, { keyframes, css } from 'styled-components';

export const Container = styled.div`
  padding: 10px;
  margin: 0 auto;
  margin-bottom: 50px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: 800px;

  form {
    display: ${props => (props.pagamentoCartao ? 'flex' : 'none')};
    flex-direction: column;
    max-width: 700px;
    width: 100%;

    span.error {
      font-size: 12px;
      margin-top: 5px;
      color: red;
    }
  }
  > p {
    font-size: 13px;
    font-style: italic;
    text-align: center;
    line-height: 1.8;
    max-width: 600px;
    padding: 30px 0;
  }
`;

export const PrimeiraMensalidade = styled.div`
  margin-top: 30px;

  div {
    width: 100%;
    display: flex;
    align-items: center;

    label {
      display: flex;
      @media (max-width: 470px) {
        flex-direction: column;
      }
      strong {
        margin: 0 10px 10px 0;
        max-width: 200px;
      }
      select {
        max-width: 150px;
        border-radius: 4px;
        border: none;
        padding: 10px;

        background: #e6e6e6;
      }
    }
    p {
      font-size: 12px;
      font-style: italic;
      max-width: 350px;
      margin: 0 15px;
    }
  }
`;

export const NomeCartao = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: column;
  @media (max-width: 470px) {
    margin-top: 10px;
  }
  label {
    display: flex;
    flex-wrap: nowrap;
    @media (max-width: 470px) {
      flex-direction: column;
      strong {
        margin: 10px 0;
      }
    }
    strong {
      margin-right: 20px;
      display: block;
      width: 150px;
    }
    input {
      border-radius: 4px;
      border: none;
      flex: 1;
      padding: 10px;
      background: #e6e6e6;
    }
  }
`;
export const Validade = styled.div`
  margin-top: 30px;
  width: 100%;
  display: flex;
  @media (max-width: 470px) {
    margin-top: 10px;

    align-items: center;
    justify-content: center;
  }
  div.validade {
    display: flex;
    flex-direction: column;
    width: 100%;
    @media (max-width: 470px) {
      width: 150px;
    }

    label {
      display: flex;
      flex-wrap: nowrap;
      @media (max-width: 470px) {
        flex-wrap: wrap;
      }
      strong {
        display: block;
        width: 150px;

        margin-right: 20px;
        @media (max-width: 470px) {
          width: 70px;
          margin: 10px 0;
        }
      }
      select {
        width: 100px;

        border-radius: 4px;
        border: none;
        padding: 10px;
        background: #e6e6e6;
      }
    }
  }

  div.validadeAno {
    display: flex;
    flex-direction: column;
    width: 100%;
    @media (max-width: 470px) {
      margin-top: 35px;
    }
    label {
      display: flex;
      align-items: center;
      flex-wrap: nowrap;
      strong {
        display: block;
        margin: 0 20px;
      }
      select {
        width: 100px;
        border-radius: 4px;
        border: none;
        padding: 10px;
        background: #e6e6e6;
      }
    }
  }
`;

export const CodVerificador = styled.div`
  margin-top: 30px;
  width: 100%;

  label {
    display: flex;
    @media (max-width: 470px) {
      flex-direction: column;
    }
    strong {
      display: block;
      width: 150px;
      margin: 0 20px 10px 0;
    }
    input {
      border-radius: 4px;
      border: none;
      flex: 1;
      padding: 10px;
      max-width: 200px;
      background: #e6e6e6;
      @media (max-width: 470px) {
        max-width: 100px;
      }
    }
  }
`;
export const Cartao = styled.div`
  margin-top: 30px;
  width: 100%;
  @media (max-width: 470px) {
    margin-top: 10px;
  }
  label {
    display: flex;
    @media (max-width: 470px) {
      flex-direction: column;
      strong {
        margin: 10px 0;
      }
    }
    strong {
      display: block;
      width: 150px;
      margin-right: 20px;
    }
    input {
      border-radius: 4px;
      border: none;
      flex: 1;
      padding: 10px;
      background: #e6e6e6;
    }
  }
  span.noPan {
    display: ${props => (!props.pan ? 'block' : 'none')};
    font-size: 12px;
    margin-top: 5px;
    color: #7f7f7f;
  }
`;
export const Contrato = styled.div`
  margin-top: 30px;
  width: 100%;
  label {
    margin-left: 170px;
    display: flex;
    align-items: center;
    @media (max-width: 470px) {
      margin-left: 0px;
    }

    input {
      margin-right: 15px;
    }
    p {
      max-width: 350px;
      font-size: 13px;
      font-style: italic;
      a {
        color: inherit;
        margin-left: 5px;
        &:hover {
          text-decoration: underline;
        }
      }
    }
  }
`;

export const Footer = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: space-between;
  width: 100%;
  @media (max-width: 800px) {
    flex-direction: column;
    align-items: center;
  }
  @media (max-width: 470px) {
    margin-top: 20px;
  }

  div.infosFinais {
    display: flex;
    flex-direction: column;

    p {
      max-width: 250px;
      font-size: 11px;
      font-style: italic;
      margin-top: 10px;
    }
  }
  div.buttons {
    display: flex;
    align-items: center;

    button.voltar {
      display: flex;
      align-items: center;
      justify-content: center;

      background: #e6e6e6;
      padding: 7.5px;
      border-radius: 4px;
      border: none;
      font-size: 14px;
      color: #7f7f7f;
      margin-right: 15px;
      transition: all 0.2s;
      &:hover {
        background: #f1f0f0;
      }
    }
  }
`;
// rotação do icone
const rotate = keyframes`
  from {
    transform:rotate(0deg);
  }
  to{
    transform: rotate(360deg);
  }
`;
export const ButtonSubmit = styled.button.attrs(props => ({
  disabled: props.loading,
}))`
  margin: 15px 0;
  width: 200px;
  padding: 10px;
  border-radius: 4px;
  font-weight: bold;
  border: none;
  background: #9fc138;
  color: #fff;
  &[disabled] {
    cursor: not-allowed;
    opacity: 0.6;
  }
  ${props =>
    props.loading &&
    css`
      svg {
        animation: ${rotate} 2s linear infinite;
      }
    `}
  &:hover {
    background: #72940c;
  }
`;

export const MetodoDePagamento = styled.div`
  border-bottom: 1px solid #7f7f7f;
  width: 100%;
  margin-top: 5px;
  display: flex;
  padding: 20px 0;
  flex-direction: column;
  input {
    margin-right: 15px;
    cursor: pointer;
  }
  a {
    margin-top: 15px;
    padding: 20px 0;
    color: inherit;
    display: flex;
    align-items: center;
    display: ${props => (!props.pagamentoCartao ? 'flex' : 'none')};
    &:hover {
      text-decoration: underline;
    }
    div {
      background: #24ca01;
      border-radius: 3px;
      display: flex;
      align-items: center;
      justify-content: center;
      margin-right: 10px;

      padding: 4px;

      img {
        height: 18px;
        width: 18px;
      }
    }
  }
`;
