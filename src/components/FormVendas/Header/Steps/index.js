import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

import { useSelector } from 'react-redux';

function getSteps() {
  return ['Selecione sua assinatura', 'Informe seus dados', 'Finalizar compra'];
}

export default function Steps() {
  const etapa = useSelector(state => state.form.etapa);
  const steps = getSteps();

  return (
    <>
      <Stepper
        style={{ background: 'transparent' }}
        activeStep={etapa}
        alternativeLabel
      >
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </>
  );
}
