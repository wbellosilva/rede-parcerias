import React from 'react';

import { useSelector } from 'react-redux';
import { Container } from './styles';
import Steps from './Steps/index';

function Header() {
  const etapa = useSelector(state => state.form.etapa);
  const h1Header = [
    'Características da assinatura',
    'Falta pouco para você se tornar um cliente Coob+',
    'Último passo para concluir o seu cadastro e começar a viajar',
  ];
  const pHeader = [
    null,
    'Agora nos informe os seus dados para cadastro',
    'Escolha uma opção de pagamento. Utilizando seu cartão de crédito Porto Plus, as parcelas serão lançadas mensalmente sem comprometer o seu limite.',
  ];

  return (
    <Container>
      <h1>{h1Header[etapa]}</h1>
      {etapa > 0 ? <p>{pHeader[etapa]}</p> : null}

      <div>
        <Steps />
      </div>
    </Container>
  );
}

export default Header;
