import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  align-items: center;
  flex-direction: column;

  h1 {
    font-size: 20px;
    text-align: center;
    max-width: 560px;
    font-weight: normal !important;
  }
  p {
    text-align: center;
    max-width: 560px;
    margin: 30px 0;

    font-size: 16px;
    color: #949494;
  }
`;
