import styled from 'styled-components';

export const Adesao = styled.div`
  display: flex;
  align-items: center;
  div#parcelaMinima {
    display: flex;
    flex-direction: column;
    justify-content: center;

    align-items: left;
    p#parcelaMinima2 {
      font-size: 12px;
      margin: 0;
    }
  }

  @media (max-width: 500px) {
    flex-direction: column;
    align-items: flex-start;
  }

  > svg {
    margin: 0 10px;

    font-size: 18px;
    @media (max-width: 500px) {
      display: none;
    }
  }
  > span {
    strong {
      color: #9fc138;
    }
  }
  div.adesao {
    display: flex;
    align-items: baseline;
    color: #333;
    span {
      margin-right: 5px;

      &:last-child {
        margin-left: 7px;
      }
    }

    div.valorDesconto {
      position: relative;
      div.desconto {
        transform: rotate(-12deg);
        top: 6px;
        right: 1px;
        position: absolute;
        width: 70px;
        height: 1.5px;
        background: red;
      }
      strong {
        display: block;
        text-align: center;
        width: 70px;
      }
    }
  }
`;
export const Mensalidade = styled.div`
  span {
    strong {
      font-size: 25px;
    }
  }
`;

export const Green = styled.span`
  color: green;
  font-weight: 800;
`;
