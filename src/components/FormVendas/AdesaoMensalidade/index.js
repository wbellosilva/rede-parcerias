import React from 'react';
import { useSelector } from 'react-redux';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import { Adesao, Mensalidade, Green } from './styles';

function AdesaoMensalidade() {
  // States do storie Redux
  const idCard = useSelector(state => state.form.idCard);
  const inicioCartao = useSelector(state => state.form.isPan);

  const valorMensalidade = Number(
    useSelector(state => state.form.valorMensalidade)
  )
    .toFixed(2)
    .toString()
    .replace('.', ',');
  const valorAdesao = Number(useSelector(state => state.form.valorAdesao))
    .toFixed(2)
    .toString()
    .replace('.', ',');
  /** API DE CONSULTA DE MENSALIDADE */
  const valorAdesaoComDesc = Number(
    useSelector(state => state.form.valorAdesaoComDesc)
  )
    .toFixed(2)
    .toString()
    .replace('.', ',');

  return (
    <>
      {idCard === '34' || idCard === '35' ? null : (
        <Adesao>
          <div id="parcelaMinima">
            <div className="adesao">
              <span>Adesão</span>
              <strong>R$ </strong>
              <div className="valorDesconto">
                <div className="desconto" />
                <strong> {valorAdesao} </strong>
              </div>
            </div>
            {/* {inicioCartao ? null : (
              <p id="parcelaMinima2">*Parcela mínima de R$ 50,00</p>
            )} */}
          </div>

          <ArrowRightAltIcon />
          {/* {inicioCartao ? (
            <span>
              <Green>0,00</Green> para clientes Clube Ben
            </span>
          ) : (
            <span>
              (menos 80% de desc.)
              <strong> R$ {valorAdesaoComDesc}</strong>
              <strong> R$ {valorAdesaoComDesc}</strong>
            </span>
          )} */}

          <span>
            <Green>R$ 0,00</Green> para Rede Parcerias
          </span>
        </Adesao>
      )}

      <Mensalidade>
        <span>
          Mensalidade 12x de <strong>R$ {valorMensalidade}</strong>
        </span>
      </Mensalidade>
    </>
  );
}

export default AdesaoMensalidade;
