import React from 'react';
import { Dropdown } from 'rsuite';

import MenuIcon from '@material-ui/icons/Menu';
import { Container, HeaderTop, HeaderInfos } from './styles';

function Header() {
  return (
    <Container className="paddingVertical">
      <div className="container">
        <HeaderTop>
          <Dropdown noCaret icon={<MenuIcon />} className="dropdown">
            <Dropdown.Item href="#sobre"> Vantagens</Dropdown.Item>
            <Dropdown.Item href="#comentarios">
              Perguntas Frequentes
            </Dropdown.Item>
            <Dropdown.Item href="#contatos">Contato</Dropdown.Item>
          </Dropdown>
          <img
            className="logo"
            src="Sections/Header/logoCoobmais.png"
            alt="Identidade visual da Coob+"
          />

          <ul>
            <li>
              <a href="#sobre">Vantagens</a>
            </li>
            <li>
              <a href="#comentarios">Perguntas Frequentes</a>
            </li>
            <li>
              <a href="#contatos">Contato</a>
            </li>
          </ul>
          <a className="btn-header" href="#formulario">
            Assine já
          </a>
        </HeaderTop>
        <HeaderInfos>
          <div className="title">
            <h1>
              <b>Com a Coob+ é possível <br />
                viajar muito mais!
              </b>
            </h1>
            <h4>
              Viaje de um jeito muito mais prático,
              <br /> flexível e econômico.
            </h4>
          </div>
          <img
            src="Sections/Header/header.png"
            alt="Imagem de família curtindo um dia na praia."
          />
        </HeaderInfos>
      </div>
    </Container>
  );
}

export default Header;
