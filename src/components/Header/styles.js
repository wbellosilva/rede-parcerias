import styled from 'styled-components';

export const Container = styled.div`
  background: url('Sections/Header/background.png');
  background-position: top left;
  background-repeat: no-repeat;
  background-size: 80% 100%;
  padding-top: 20px;

  @media (min-width: 1200px) {
    background-size: 74% 100%;
  }
  @media (min-width: 1450px) {
    background-size: 70% 100%;
  }
  @media (max-width: 800px) {
    background-size: 85% 100%;
  }
  @media (max-width: 990px) {
    background-size: cover;
    height: 700px;
    background-position: top;
  }
`;
export const HeaderTop = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  .logo{
    width: 250px;
  }
  
  @media (max-width: 600px) {
    img.logo {
      width: 150px;
    }
  }
  @media (max-width: 990px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
    div.dropdown {
      margin-left: 15px;
      a {
        background: #fff;
        display: flex;
        align-items: center;
      }
    }
  }
  @media (min-width: 990px) {
    div.dropdown {
      display: none;
    }
  }
  ul {
    display: flex;
    align-items: center;
    justify-content: center;
    li {
      margin: 10px;
      a {
        color: #fff;
        border-bottom: 2px transparent solid;
        padding: 5px;
        &:hover {
          border-color: transparent;
        }
      }
    }
  }

  @media (max-width: 990px) {
    padding: 10px;
    ul {
      display: none;
    }
  }
  .btn-header {
    padding: 10px 20px;
    border-radius: 4px;
    color: #fff;
    font-weight: bold;
    background: #f45c08;
  }
`;
export const HeaderInfos = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 990px) {
    flex-direction: column-reverse;
    justify-content: center;

    img {
      max-width: 300px;
      margin: 15px 0;
    }
    div.title {
      h1 {
        line-height: 1;
      }
    }
  }

  div.title {
    color: #fff;
    h1 {
      font-size: 35px;
      font-weight: 500;
      margin-bottom: 16px;
      line-height: 1.3;
      @media (max-width: 990px) {
        font-size: 30px;
      }
      @media (max-width: 900px) {
        font-size: 25px;
      }
    }
    h4 {
      font-size: 25px;
      font-weight: 500;
      @media (max-width: 900px) {
        font-size: 20px;
      }
      @media (max-width: 990px) {
        font-size: 15px;
      }
    }
  }
`;
