import React from 'react';

import { Container } from './styles';

function Video() {
  return (
    <Container>
      <div>
        <a
          className="whatsapp-tel"
          href="https://api.whatsapp.com/send?phone=555474008256"
          target="_blank"
        >
          <img
            src="Sections/Video/contatoTelefone.png"
            alt="Imagem contendo um número de contato, 0300 647 4425"
          />
        </a>
        <iframe
          title="Video COOB+"
          width="560"
          height="315"
          src="https://www.youtube.com/embed/ROdwozraLtA"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    </Container>
  );
}

export default Video;
