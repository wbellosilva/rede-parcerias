import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  div {
    display: flex;
    justify-content: center;
    position: relative;
    width: 1000px;
    @media (max-width: 800px) {
      width: 100%;
    }
    iframe {
      @media (max-width: 800px) {
        margin: 10px;
      }
      border-radius: 8px;
      @media (max-width: 800px) {
        height: 250px;
      }
      @media (max-width: 450px) {
        height: 200px;
      }
    }
    a {
      img {
        position: absolute;
        top: 50px;
        right: 120px;
        @media (max-width: 950px) {
          display: none;
        }

        animation-timing-function: cubic-bezier(.36,.07,.19,.97);
        animation-name: shake;
        animation-duration: 4.72s;
        animation-fill-mode: both;
        animation-iteration-count: infinite;
        animation-delay: 0.5s;
      }
    }
  }

  @keyframes shake {
    0% {
      transform:translate(0,0) rotate(0deg) scale(1);
    }
    1.78571% {
      transform:translate(5px,0) rotate(0deg);
    }
    3.57143% {
      transform:translate(0,0) rotate(5deg);
    }
    5.35714% {
      transform:translate(-5px,0) rotate(-5deg);
    }
    7.14286% {
      transform:translate(0,0) rotate(5deg);
    }
    8.92857% {
      transform:translate(5px,0) rotate(-5deg);
    }
    10.71429% {
      transform:translate(0,0) rotate(0deg) scale(1);
    }
    50% {
      transform:scale(1.05);
    }
    100% {
      transform:translate(0,0) rotate(0deg) scale(1);
    }
 }
`;
