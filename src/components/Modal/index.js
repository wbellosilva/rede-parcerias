import React from 'react';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Button } from './styles';
import { modal } from '../../store/modules/form/actions';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    border: 'none',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

function ModalError() {
  const dispatch = useDispatch();

  const modalIsOpen = useSelector(state => state.form.modalIsOpen);
  const messageModal = useSelector(state => state.form.messageModal);

  function closeModal() {
    dispatch(modal(false, null));
  }
  return (
    <div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <Container>
          <main>
            <div>
              <span>:(</span>
            </div>
            <strong>Ops</strong>
            <p>{messageModal}</p>
          </main>
          <Button type="button" onClick={closeModal}>
            Fechar
          </Button>
        </Container>
      </Modal>
    </div>
  );
}

export default ModalError;
