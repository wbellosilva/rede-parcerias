import styled from 'styled-components';

export const Container = styled.div`
  min-width: 400px;
  min-height: 300px;
  @media (max-width: 735px) {
    max-width: 380px;
    min-width: 250px;
    min-height: 300px;
  }
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  main {
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  div {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background: #c70039;
    margin-bottom: 15px;
    span {
      color: #fff;
      font-weight: bold;
      transform: rotate(90deg);
    }
  }
  p {
    margin: 30px 0;
    text-align: center;
  }
`;
export const Button = styled.button`
  margin: 0 auto;
  padding: 15px;
  background: #c70039;
  font-weight: bold;
  border: none;
  border-radius: 3px;
  color: #fff;
  min-width: 200px;
  &:hover {
    background: #943126;
  }
`;
