import React from 'react';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Button } from './stylesPorto';
import { modalPorto } from '../../store/modules/form/actions';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    border: 'none',
    bottom: 'auto',
    padding: `none`,
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

function ModalPortoPlus() {
  const dispatch = useDispatch();

  const PortoModal = useSelector(state => state.form.PortoModal);

  function closeModal() {
    dispatch(modalPorto(false));
  }
  return (
    <div>
      <Modal
        isOpen={PortoModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <Container>
          <main>
            <strong>VOCÊ GANHOU</strong>
            <strong className="destaque">100% DE DESCONTO</strong>
            <strong>
              NA ADESÃO! ESSA É UMA
              <br />
              VANTAGEM EXCLUSIVA <br />
              DO SEU CARTÃO DE CRÉDITO
              <br />
              PORTO SEGURO!
            </strong>
          </main>
          <Button type="button" onClick={closeModal}>
            Continuar
          </Button>
        </Container>
      </Modal>
    </div>
  );
}

export default ModalPortoPlus;
