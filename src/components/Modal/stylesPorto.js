import styled from 'styled-components';

export const Container = styled.div`
  min-width: 360px;
  min-height: 280px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #00afff;
  padding: 10px;
  main {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    strong {
      text-align: center;
      font-size: 18px;
      color: #fff;
    }
    .destaque {
      color: #0d243c;
    }
  }
`;
export const Button = styled.button`
  margin-top: 15px;
  padding: 10px;
  background: #0d243c;
  font-weight: bold;
  border: none;
  border-radius: 3px;
  color: #fff;
  min-width: 200px;
`;
