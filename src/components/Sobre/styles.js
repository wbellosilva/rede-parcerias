import styled from 'styled-components';

export const Container = styled.section`
  margin-top: -20px;
  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    @media (max-width: 990px) {
      justify-content: center;
      flex-direction: column;
    }
    div.infosText {
      @media (max-width: 800px) {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }
      h3 {
        @media (max-width: 800px) {
          text-align: center;
          font-size: 35px;
        }
        font-size: 46px;
        color: #00b0ff;
        font-weight: 900;
        margin: 16px 0;
        line-height: 1.05;
      }
      p {
        font-size: 20px;
        font-weight: bold;
        @media (max-width: 800px) {
          text-align: center;
          font-size: 16px;
          margin-bottom: 15px;
        }
      }
    }
    ul {
      display: grid;
      grid-template-columns: 200px 200px;
      @media (max-width: 990px) {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
      }
      li {
        margin: 10px;
        white-space: nowrap;
        p {
          margin-top: 10px;
        }
      }
    }
  }
  .detalhe-promocao {
    font-size: 12px !important;
    margin-top: 10px;
    font-weight: 500;
  }
  .pt-20 {
    padding-top: 20px;
  }
`;
