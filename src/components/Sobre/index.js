import React from 'react';

import { Container } from './styles';

function Sobre() {
  return (
    <Container>
      <div className="container paddingVertical pt-20">
        <div className="infosText">
          <img
            src="Sections/Sobre/logoParceiro.png"
            alt="Logo Porto Plus e Porto Seguro."
          />
          <h3>
            Descontos exclusivos
            <br />
            para Rede Parcerias
          </h3>
          <p>
            Desconto de 100% no valor da adesão da assinatura
            ao plano de hospedagem escolhido + o desconto de
            100% na 1ª mensalidade + 2,5% de desconto real
            da 2ª a 24ª mensalidade.
          </p>
          <p>
            Faça a sua assinatura de viagens Coob+<br />
            e não perca essa vantagem!
          </p>

          <p className="detalhe-promocao">
            {/* * Promoção válida nos primeiros 90 dias. */}
          </p>
        </div>
        <ul>
          <li>
            <img
              src="Sections/Sobre/diarias.png"
              alt="ícone representando as diarias."
            />
            <p>
              Economia de até 60%
              <br />
              da tarifa balcão em
              <br />
              mais de 1800 opções
              <br />
              de hospedagem
            </p>
          </li>
          <li>
            <img src="Sections/Sobre/digital.png" alt="ícone de um notebook." />
            <p>
              Escolha e reserve um <br />
              hotel pelo Reserva <br />
              Fácil no site <br />
              Coobmais.com
            </p>
          </li>
          <li>
            <img
              src="Sections/Sobre/seguro.png"
              alt="ícone do mapa do Brasil."
            />
            <p>
              Seguro viagem
              <br />
              nacional incluso
            </p>
          </li>
          <li>
            <img
              src="Sections/Sobre/calendario.png"
              alt="ícone de calendário."
            />
            <p>
              Diárias com até 3 anos
              <br />
              de validade pra você
              <br />
              utilizar em suas viagens
            </p>
          </li>
        </ul>
      </div>
    </Container>
  );
}

export default Sobre;
