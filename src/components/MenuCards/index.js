import React from 'react';
import CardPlanos from '../CardPlanos/index';
import { Section } from './styles';
import { Planos } from '../../Data/Planos';

function MenuCards() {
  return (
    <Section>
      <div className="planos">
        <ul>
          {Planos.map(obj => (
            <li key={obj.id}>
              <CardPlanos Plano={obj} />
            </li>
          ))}
        </ul>
      </div>
      <h2>
        Rede Parcerias ganha descontos especiais!
      </h2>
    </Section>
  );
}

export default MenuCards;
