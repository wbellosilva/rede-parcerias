import styled from 'styled-components';

export const Section = styled.section`
  div.planos {
    margin: 0 auto;
    max-width: 1000px;
    padding-top: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    h3 {
      max-width: 1100px;
      font-size: 32px;
      color: #fff;
      text-align: center;
      @media (max-width: 900px) {
        font-size: 25px;
      }
    }
    > span {
      margin: 25px 0;
      color: #fff;
    }
    ul {
      display: flex;
      width: 100%;
      align-items: center;
      justify-content: space-between;
      flex-wrap: wrap;
      @media (max-width: 900px) {
        justify-content: center;

        li {
          margin: 10px;
        }
      }
      li {
        margin-right: 10px;
        &:last-child {
          margin-right: 0;
        }
      }
    }
  }
  @media (max-width: 900px) {
    h2 {
      font-size: 25px;
    }
  }
  @media (max-width: 650px) {
    h2 {
      font-size: 18px;
    }
  }
  h2 {
    font-size: 28px;
    margin: 30px auto;
    text-align: center;
    line-height: 0.95;
  }
`;
