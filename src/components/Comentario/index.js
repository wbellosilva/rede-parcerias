import React from 'react';
import { Container } from './styles';

function Comentario() {
  return (
    <Container>
      <div>
        <h3>
          Faça como os nossos mais de 60mil
          <br />
          Viajantes Coob+!
        </h3>
        <p>
          A Coob+ tem proporcionado momentos inesquecíveis junto a minha
          <br />
          família e amigos. Vale muito a pena, pois trata-se de um investimento
          <br />
          suave que trás retornos extraordinários em satisfação, bem estar e
          <br />
          lembranças inapagáveis!
        </p>
        <strong>André Alencar - #ViajanteCoob+</strong>
      </div>
    </Container>
  );
}

export default Comentario;
