import styled from 'styled-components';

export const Container = styled.div`
  padding: 10px;
  > div {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    margin: 40px auto;
    text-align: center;
    background: url('Sections/Depoimentos/divImg.png');
    background-position: center;
    background-repeat: no-repeat;
    border-radius: 15px;
    width: 999px;
    box-shadow: 5px 4px 16px 3px rgba(0, 0, 0, 0.22);
    height: 270px;
    @media (max-width: 1000px) {
      margin: 10px auto;
      width: 700px;
    }
    @media (max-width: 800px) {
      width: 100%;
      padding: 10px;
      background-size: cover;
    }
    @media (max-width: 500px) {
      padding: 10px;
      height: 100%;
      h3 {
        font-size: 10px;
      }
      p {
        font-size: 14px;
        text-align: center;
      }
    }

    h3 {
      line-height: 1.2;
      font-size: 30px;
      color: #00afff;
      @media (max-width: 800px) {
        font-size: 20px;
      }
    }
    p {
      margin: 15px 0;
      font-size: 18px;
      @media (max-width: 800px) {
        font-size: 16px;
      }
    }
    strong {
      font-size: 18px;
    }
  }
`;
