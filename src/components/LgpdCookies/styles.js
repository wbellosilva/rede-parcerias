import styled from 'styled-components';

export const Container = styled.div`
    margin: 0 auto;

    .alert-container {
        display: none;
        bottom: 0px;
        left: 0px;
        width: 100%;
        z-index: 100;
        padding-bottom: 40px;
    }
    .alert {
        margin-bottom: 0;
    }
    .alert p {
        text-align: center;
        font-size: 12px !important;
        color: black;
    }
    .btn-black {
        background-color: black;
        color: white;

        @media(max-width: 768px) {
            font-size: small;
            margin-top: 10px;
        }
    }
    .btn-black:hover {
        color: rgb(252, 176, 52);
    }
`;