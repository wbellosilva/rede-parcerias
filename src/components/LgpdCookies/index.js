/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';

import { Container } from './styles';

export default class LgpdCookies extends React.Component {

    componentDidMount() {
        this.verificaCookies();   
    }

    mostrarAceite() {
        const alerta = document.getElementById('alerta-cookies');
    
        alerta.style.position = 'fixed';
        alerta.style.display = 'block';
        alerta.style.bottom = '0';
    }

    salvarAceite() {
        const objeto = {
            aceite: true,
            data: new Date()
        }

        try {
            localStorage.setItem('aceite-cookies-rede-parcerias', JSON.stringify(objeto));
            const alerta = document.getElementById('alerta-cookies');
            alerta.remove();
        } catch (error) {

        }
    }

    verificaCookies() {
        const aceite = localStorage.getItem('aceite-cookies-rede-parcerias');
    
        if (aceite) {
            const objeto = JSON.parse(aceite);
    
            if(objeto.aceite && objeto.data) return false;
    
        }
        this.mostrarAceite();
        return true;
    }

    render() {

        return (
            <Container>
                <div className="alert-container alert alert-warning" id="alerta-cookies">
                    <div className="row">
                            <div className="col-md-9">
                                <p>Usamos cookies somente para melhorar sua experiência no site. Ao continuar navegando, você concorda com nossa {' '}
                                    <a className="alert-link" href="https://www.coobmais.com.br/politica-cookies.html" target="_blank">política de cookies</a> e com a <a className="alert-link"
                                    href="https://www.coobmais.com.br/politica-privacidade.html" target="_blank">política de privacidade</a>
                                </p>
                            </div>
                            <div className="col-md-3">
                                <button className="btn btn-black" onClick={this.salvarAceite}>Continuar e fechar</button>
                            </div>
                    </div>
                </div>
            </Container>
        );    
    }
}
