import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

  *{
    margin:0;
    padding:0;
    outline:0;
    box-sizing:border-box;
  }
  *:focus{
    outline:0;
  }
  html, body , #hoot{
    height:100%;
    scroll-behavior: smooth;


  }

  body{
    -webkit-font-smoothing: antialiased;

  }
  body, input, button{
    font-size: 16px;
    font-family: 'Montserrat';
    color:#0D243C;
    @media (max-width: 900px) {
    font: 14px 'Montserrat';
    }
  }

  a{
    text-decoration:none;
    color:inherit;
  }
  ul{
    list-style:none;
  }
  a, button{
    cursor:pointer;
  }

  .container{
    margin: 0 auto;
    width: 100%;
    max-width: 1000px;
  }

 .imageBackgroundMeio{
  background: url('Sections/Planos/bgmeio.png');
  background-position: right center;
  background-repeat: no-repeat;
  background-size: 25% 1000px;
 
  @media (min-width: 1600px) {
    background:#fff;
  }


 }
 .imageBackgroundFinal{
  background: url('Sections/Depoimentos/bgDepoimento.png');
  background-position:top 200px left ;
  background-repeat: no-repeat;
  background-size: 50% 800px;
 }
`;
