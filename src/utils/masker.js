export const masker = (value, mask) => {
  if (!value) return '';

  const exp = /-|\.|\/|\(|\)|/g;
  const numbers = value.toString().replace(exp, '');

  if (numbers < 1) return;

  let index = 0;
  let maskedValue = '';
  let digits = numbers.length;

  for (let i = 0; i <= digits; i++) {
    const isMask = Boolean(
      mask.charAt(i) == '-' ||
      mask.charAt(i) == '.' ||
      mask.charAt(i) == '/' ||
      mask.charAt(i) == '(' ||
      mask.charAt(i) == ')' ||
      mask.charAt(i) == ' '
    );

    maskedValue += isMask ? mask.charAt(i) : numbers.charAt(index);

    if (!isMask) index++;
    if (isMask) digits++;
  }

  return maskedValue;
};
