// eslint-disable-next-line consistent-return
function idCardString(id) {
  switch (id) {
    case '35':
      return 'GO! Master';
    case '34':
      return 'GO! Vip';
    case '21':
      return 'Vip';
    case '22':
      return 'Master';
    case '31':
      return 'Gold Vip';
    case '32':
      return 'Gold Master';
    case '33':
      return 'Diamante';
    default:
  }
}
export default idCardString;
