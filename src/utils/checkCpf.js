export const checkCpf = cpf => {
  const strCPF = cpf.replace(/[^\d]+/g, '');

  let Soma;
  let Resto;

  Soma = 0;
  if (strCPF == '00000000000') return false;

  for (let n = 1; n <= 9; n++)
    Soma += parseInt(strCPF.substring(n - 1, n)) * (11 - n);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(9, 10))) return false;

  Soma = 0;
  for (let s = 1; s <= 10; s++)
    Soma += parseInt(strCPF.substring(s - 1, s)) * (12 - s);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(strCPF.substring(10, 11))) return false;
  return true;
};
