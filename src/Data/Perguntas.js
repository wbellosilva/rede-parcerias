/* eslint-disable react/no-unescaped-entities */
import React from 'react';

const resposta1 = (
  <>
    <p>
      Ao adquirir uma assinatura Coob+, você está fazendo uma Assinatura de
      Viagem. A cada ano será disponibilizado um lote de diárias hoteleiras, de
      acordo com a assinatura e quantidade que você escolher, podendo ser utilizado
      nos diversos hotéis parceiros credenciados em todo o Brasil.
    </p>
    <p>
      As diárias são disponibilizadas virtualmente na secção “Minha Conta” no
      site da Coob+.
    </p>
    <p>
      As assinaturas Coob+: Vip, Master, Gold e Diamante oferecem de 07 até 49
      diárias anuais, todas distribuídas em blocos, denominados de e-tickets, de
      3 (três) e de 2 (duas) diárias, que, somados, perfarão o montante
      correspondente ao número total de diárias da opção da assinatura adquirida.
    </p>
    <p>Todas as diárias nacionais contam com café da manhã incluso.</p>
  </>
);
const resposta2 = (
  <>
    <p>
      As opções de assinaturas Coob+ oferecem hospedagem em quartos duplos, que
      acomodam até 2 pessoas e em quartos triplos que acomodam até 3 pessoas,
      denominado assinatura família. As denominações “vip” e “master” diferenciam o
      período de utilização das hospedagens. Conforme a rede de hotéis
      parceiros, são oferecidas 3 categorias de assinatura: Vip/Master, Gold e
      Diamante.
    </p>

    <p>
      <strong>Assinatura: Vip </strong>- Utilização o ano todo – rede de hotéis
      executiva ou convencional - opção de assinatura para quartos duplos e triplos
    </p>
    <p>
      <strong>Assinatura: Master </strong>- Utilização 15/03 a 15/12 – rede de hotéis
      executiva ou convencional - opção de assinatura para quartos duplos e triplos
    </p>
    <p>
      <strong>Assinatura: Gold Vip </strong>- Utilização o ano todo – rede de hotéis
      superior - opção de assinatura para quartos duplos e triplos
    </p>
    <p>
      <strong>Assinatura: Gold Master </strong>- Utilização 15/03 a 15/12 – rede de
      hotéis superior - opção de assinatura para quartos duplos e triplos
    </p>
    <p>
      <strong>Assinatura: Diamante </strong>- Utilização o ano todo – rede de hotéis
      super luxo e resort - opção de assinatura para quartos duplos e triplos
    </p>
  </>
);
const resposta3 = (
  <>
    <p>
      O pagamento da assinatura se dá por meio de mensalidades. Assim que é realizada
      a assinatura, é disponibilizado o número de diárias contratadas,
      que serão pagas em 12 mensalidades. A cada 12 mensalidades pagas,
      completa-se um período da assinatura (1 ano) e inicia-se o próximo, quando um
      novo lote de diárias são disponibilizadas.
    </p>
    <p>
      As mensalidades podem ser pagas por meio de cartão de crédito e débito em
      conta, nos principais bancos.{' '}
      <strong>
        Os valores são descontados mês a mês, e não comprometem o limite do
        cartão.
      </strong>
    </p>
  </>
);
const resposta4 = (
  <>
    <p>
      As assinaturas: Master, Vip, Gold e Diamante têm um quantitativo de 07 até 49
      diárias/ano para duas ou três pessoas.
    </p>
    <p>
      Cada cliente pode adquirir até 05 (cinco) assinaturas, podendo ultrapassar este
      número mediante consulta.
    </p>
  </>
);
const resposta5 = (
  <>
    <p>
      É a representação virtual das diárias da sua assinatura. Os e-tickets possuem
      validade de 03 anos e são apresentados de duas formas: e-ticket de 3
      (três) e de 2 (duas) diárias. Não há e-tickets de apenas 1 (uma) diária.
      Apresentam-se com um código de segurança para validarem a reserva e
      emissão do voucher de hospedagem.
    </p>
    <p>
      Quando o Viajante Coob+ realizar a aquisição da assinatura, receberá um
      Extrato contendo os e-tickets com as respectivas diárias de acordo com o
      tipo de assinatura escolhida. Além do extrato enviado no início da assinatura, o
      viajante recebe anualmente o controle de movimentação dos seus e-tickets e
      seus respectivos códigos de segurança, bem como, um aviso anual com 3
      (três) meses de antecedência, quando as diárias estão próximas de vencer.
      O viajante Coob+ também pode, a qualquer tempo, fazer a consulta
      logando em sua conta no site{' '}
      <a href="https://www.coobmais.com/"> coobmais.com</a>
    </p>
  </>
);
const resposta6 = (
  <>
    <p>
      A validade inicia 15 dias após o pagamento da 1ª mensalidade. Ex. 1ª
      mensalidade débito em conta no dia 10 do mês, a validade do e-ticket será
      a partir do dia 25.
    </p>
  </>
);
const resposta7 = (
  <>
    <p>
      Não, elas podem ser utilizadas separadamente. A quantidade de diárias da
      assinatura é disponibilizada em e-tickets de 2 ou de 3 diárias. Na assinatura de 7
      diárias, por exemplo, é disponibilizado 1 e-ticket de 3 diárias e 2
      e-tickets de 2 diárias, que podem ser utilizados de uma única vez no mesmo
      hotel, de uma única vez em diferentes hotéis ou em diferentes viagens,
      sempre mantendo o mínimo de 2 diárias no hotel.
    </p>
  </>
);

const resposta9 = (
  <>
    <p>
      A validade de todos e-tickets é de 3 (três) anos. As diárias não
      utilizadas dentro deste período, não podem ser mais utilizadas.
    </p>
  </>
);
const resposta10 = (
  <>
    <p>
      Todos os hotéis credenciados Coob+, estão disponíveis para
      visualização no site <a style={{color: 'blue'}} href="https://coobmais.com" rel="nofollow" target="_blank">coobmais.com</a> onde, no campo de pesquisa, você
      pode filtrar por região, nome de hotel e cidade. Por meio da plataforma
      ‘Reserva Fácil’ você pode fazer as reservas de forma facilitada e
      totalmente online.
    </p>
  </>
);
const resposta11 = (
  <>
    <p>
      Sim. O e-Ticket Coob+ pode ser utilizado por outras pessoas que não o
      titular. Mas, para a utilização, o terceiro deverá ter em mãos uma
      autorização por escrito, com assinatura do titular do assinatura, reconhecida
      em cartório, autorizando o uso do e-ticket referido, a qual deverá ser
      entregue ao hotel no momento da hospedagem.
    </p>
    <p>
      O formulário de Autorização fica disponível na plataforma “Reserva Fácil”,
      na área logada do viajante no site
      <a href="https://www.coobmais.com/"> Coobmais.com</a>
    </p>
  </>
);
const resposta12 = (
  <>
    <p>
      A reserva é feita pelo próprio assinante, utilizando o Sistema de ‘Reserva
      Fácil’ no site da Coob+. O usuário precisa acessar o site
      Coob+.com , clicar em Reserva Fácil e seguir os seguintes passos:
    </p>
    <p>
      A. Selecione o hotel desejado ou, se preferir, a cidade onde deseja se
      hospedar, para fazer a busca por região.
    </p>
    <p>B. Selecione as datas desejadas.</p>
    <p>
      C. Selecione o número de quartos e a quantidade de pessoas que irão se
      hospedar.
    </p>
    <p>
      D. Insira o login e a senha ( os mesmos de acesso a conta do assinante) .
    </p>
    <p>E. Defina os e-Tickets que serão utilizados na reserva.</p>
    <p>
      F. Caso haja valores adicionais, como por exemplo, pacote extra, cama
      adicional e entre outros, inserir as informações de pagamento.
    </p>
    <p>G. Confirme a reserva.</p>
    <p>H. Pronto! A reserva é confirmada com o voucher no seu e-mail.</p>
    <p>
      Após realizar o pagamento, via e-ticket e ou cartão, o assinante receberá
      um e-mail com a confirmação da reserva.
    </p>
    <p>
      O assinante tem total autonomia para escolher o destino e o hotel
      desejado, sendo importante ficar atento ao período de uso, a rede de
      hotéis estabelecidas pela sua assinatura e sempre respeitando a antecedência
      mínima de 30 dias antes da entrada no hotel. Caso o período que antecede a
      solicitação seja menor do que 30 dias, a reserva só poderá ser realizada
      mediante solicitação de serviço das nossas Agências de Viagens parceiras.
    </p>
  </>
);
const resposta13 = (
  <>
    <p>
      A sua Assinatura de Hospedagem tem validade indeterminada. A cada 12 (doze)
      meses, a contar da data do pagamento da primeira mensalidade, é concluído
      um ciclo e automaticamente iniciado o próximo, gerando novo lote de
      e-tickets, sem o pagamento de nenhuma taxa ou adesão adicional, caso não
      haja manifestação em contrário.
    </p>
  </>
);
const resposta14 = (
  <>
    <p>
      Para realizar o cancelamento das assinaturas Coob+ é sempre bom se atentar
      as premissas de trinta dias antes do próximo débito. A assinatura não pode
      estar com inadimplência de mensalidade ou e-tickets utilizados.
    </p>
  </>
);
const resposta15 = (
  <>
    <p>
      No momento da sua reserva no sistema "Reserva Fácil", você já visualizará
      a tela de contratação do seguro. Os seus dados, como titular da assinatura, já
      estarão preenchidos, mas deverá, ainda, preencher os dados do seu
      acompanhante. O seguro do titular e do acompanhante é um benefício
      oferecido pela Coob+, gratuitamente.
    </p>
    <p>
      A apólice do seguro é emitida 4 dias antes da data do check-in e enviada
      por e-mail, além de ficar disponível na área logada do titular.
    </p>
    <p>
      Para maiores informações<a href="https://bit.ly/3hVcE0L" className="link"> clique aqui.</a>
    </p>
  </>
);

const resposta16 = (
  <>
    <p>
      As regras de utilização das diárias com menos de 30 dias foram atualizadas. 
    </p>
    <p>
      Para saber mais, <a href="https://www.coobmais.com/info.html#/ajuda-info" className="link" target="_blank" rel="nofollow"> clique aqui.</a>
    </p>
  </>
);

const resposta17 = (
  <>
    <p>
      As regras de reembolso fora da rede foram atualizadas.
    </p>
    <p>
      Para saber mais, <a href="https://www.coobmais.com/info.html#/ajuda-info" className="link" target="_blank" rel="nofollow"> clique aqui.</a>
    </p>
  </>
);

export const Perguntas = [
  {
    id: '1',
    pergunta: 'O que você recebe quando adquire uma assinatura de hospedagem?',
    resposta: resposta1,
  },
  {
    id: '2',
    pergunta:
      'Quais são as assinaturas de hospedagem da Coob+ e quais suas características?',
    resposta: resposta2,
  },
  {
    id: '3',
    pergunta: 'Quais são as formas de pagamento das assinaturas Coob+?',
    resposta: resposta3,
  },
  {
    id: '4',
    pergunta: 'Quantas são as diárias por assinatura?',
    resposta: resposta4,
  },
  {
    id: '5',
    pergunta: 'O que é um e-Ticket Coob+?',
    resposta: resposta5,
  },
  {
    id: '6',
    pergunta:
      'A partir de que data posso utilizar minhas diárias após a aquisição da assinatura?',
    resposta: resposta6,
  },
  {
    id: '7',
    pergunta: 'As diárias precisam ser utilizadas todas de uma única vez?',
    resposta: resposta7,
  },

  {
    id: '9',
    pergunta: 'Qual é a validade das diárias da assinatura?',
    resposta: resposta9,
  },
  {
    id: '10',
    pergunta: 'Onde consultar os hotéis credenciados?',
    resposta: resposta10,
  },
  {
    id: '11',
    pergunta: 'Posso ceder meus e-tickets para terceiros?',
    resposta: resposta11,
  },
  {
    id: '12',
    pergunta: 'Como faço uma reserva?',
    resposta: resposta12,
  },
  {
    id: '13',
    pergunta: 'Como é feita a renovação da sua Assinatura de Hospedagem?',
    resposta: resposta13,
  },
  {
    id: '14',
    pergunta: 'Posso cancelar minha assinatura a qualquer momento?',
    resposta: resposta14,
  },
  {
    id: '15',
    pergunta:
      'O seguro viagem está incluso em todas as assinaturas Coob+. Em que momento eu solicito o seguro?',
    resposta: resposta15,
  },
  {
    id: '16',
    pergunta:
      'Reserva com menos de 30 dias de antecedência',
    resposta: resposta16,
  },
  {
    id: '17',
    pergunta:
      'Reembolso fora da rede',
    resposta: resposta17,
  },
];
