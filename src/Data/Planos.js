export const Planos = [
  {
    nome: 'Master',
    src: 'Sections/Planos/plano-master.png',
    utilizacao: '15/03 à 15/12',
    id: '22',
    categoria: 'Convencional',
    diarias: '7',
    background: 'rgb(0, 176, 255)',
    valorAdesao: '0,00',
  },
  {
    nome: 'Vip',
    src: 'Sections/Planos/plano-vip.png',
    utilizacao: 'Ano todo',
    id: '21',
    categoria: 'Superior',
    diarias: '7',
    background: 'rgb(0, 176, 255)',
    valorAdesao: '0,00',
  },
  {
    nome: 'GoldMaster',
    src: 'Sections/Planos/plano-goldmaster.png',
    utilizacao: '15/03 à 15/12',
    id: '32',
    categoria: 'Superior',
    diarias: '7',
    background: 'rgb(245, 196, 6)',
    valorAdesao: '0,00',
  },
  {
    nome: 'GoldVip',
    src: 'Sections/Planos/plano-goldvip.png',
    utilizacao: 'Ano Todo',
    id: '31',
    categoria: 'Superior',
    diarias: '7',
    background: 'rgb(245, 196, 6)',
    valorAdesao: '0,00',
  },
  {
    nome: 'Diamante',
    src: 'Sections/Planos/plano-diamante.png',
    utilizacao: 'Ano Todo',
    id: '33',
    categoria: 'Super luxo e Resorts',
    diarias: '7',
    background: 'rgb(46, 9, 156)',
    valorAdesao: '0,00',
  },
];

export const bandeiras = [
  {
    src: 'cartoes/visa.png',
    id: '1',
    alt: 'Bandeira Visa',
  },
  {
    src: 'cartoes/credicard.png',
    id: '2',
    alt: 'Bandeira Credicard',
  },
  {
    src: 'cartoes/master.png',
    id: '3',
    alt: 'Bandeira Master',
  },
  {
    src: 'cartoes/diners.png',
    id: '4',
    alt: 'Bandeira Diners',
  },
  {
    src: 'cartoes/american.png',
    id: '6',
    alt: 'Bandeira American Express',
  },

  {
    src: 'cartoes/hipercard.png',
    id: '7',
    alt: 'Bandeira Hipercard',
  },
  {
    src: 'cartoes/discover.png',
    id: '9',
    alt: 'Bandeira Discover',
  },
  {
    src: 'cartoes/aura.png',
    id: '10',
    alt: 'Bandeira Aura',
  },
  {
    src: 'cartoes/elo.jpg',
    id: '8',
    alt: 'Bandeira Elo',
  },
  {
    src: 'cartoes/sicredi.png',
    id: '11',
    alt: 'Bandeira Sicredi',
  },
];
