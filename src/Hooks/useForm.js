import React from 'react';
import { checkCpf } from '../utils/checkCpf';

const types = {
  email: {
    regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    message: 'Preencha um email válido.',
  },
};

function useForm(type) {
  const [value, setValue] = React.useState('');
  const [error, setError] = React.useState(null);

  function validate(valueInput) {
    if (type === false) return true;
    if (valueInput.length === 0) {
      setError('Preencha um valor.');
      return false;
    }
    // Validar o nome < 3
    // validar o sobrenome >3
    if (type === 'nome' && valueInput.length < 3) {
      setError('Preencha corretamente.');
      return false;
    }
    if (type === 'dados' && valueInput.length < 3) {
      setError('Preencha corretamente.');
      return false;
    }

    if (types[type] && !types[type].regex.test(valueInput)) {
      setError(types[type].message);
      return false;
    }
    setError(null);
    return true;
  }
  function onChange({ target }) {
    if (error) validate(target.value);

    setValue(target.value);
  }

  return {
    value,
    setValue,
    onChange,
    error,
    validate: () => validate(value),
    onBlur: () => validate(value),
  };
}
export default useForm;
