import axios from 'axios';

const api = axios.create({
  baseURL: 'https://www.coobmais.com.br/wsMotorVendas/',
});
export default api;

/*
 * https://www.coobmais.com.br/wsMotorVendas/
 * http://10.1.2.174/site.coobrastur.com/wscoobrastur/
 *
 */
