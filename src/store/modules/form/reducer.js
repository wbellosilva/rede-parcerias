import produce from 'immer';

export const INITIAL_STATE = {
  codSiteVendedor: '99012',
  codigoVendedorIS: '',
  modalIsOpen: false,
  messageModal: null,
  // state cards
  in_plFamilia: '0',
  in_diarias: '7',
  in_diariasGO: '4',
  in_mentpCodigoAdesao: 1,
  in_mentpCodigoMensalidade: 2,
  valorAdesaoComDesc: '',

  // ids cards
  idVip: '21',
  idMaster: '22',
  idGoldVip: '31',
  idGoldMaster: '32',
  idDiamante: '33',
  idGo: '35',

  etapa: 0,
  idCard: '31',
  idCartaoCredito: null,
  erroCartao: false,
  diarias: '7',
  familia: '0',
  valorMensalidade: '0',
  totalMensalidade: '0',
  valorAdesao: '',
  adesao: 2,
  token: null,
  responseRequest: 'SUCESSO',
  infosUser: {
    nome: '',
    cpf: '',
    ddd: '',
    celular: '',
    uf: '',
    email: '',
  },
  numPedido: null,
  restricaoSPC: null,
  PortoModal: false,
  isPan: true,
  binPan: [
    '536380',
    '536537',
    '532930',
    '553659',
    '512452',
    '519873',
    '484635',
    '412177',
    '415274',
    '415275',
    '446690',
    '462068',
  ],
  urlParams: {
    utm_source: null,
    utm_medium: null,
    utm_campaign: null,
    $idvend: null,
  },
};
export default function form(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@next/PAGE': {
        if (draft.etapa < 2) {
          draft.etapa += 1;
        }
        break;
      }
      case '@back/PAGE': {
        if (draft.etapa > 0) {
          draft.etapa -= 1;
          if (draft.isPan) {
            draft.isPan = true;
          }
        }

        break;
      }
      case '@change/CARD': {
        const { id } = action.payload;

        // verifica o stepForm
        if (draft.etapa > 0) {
          draft.etapa = 0;
          if (draft.isPan) {
            draft.isPan = true;
          }
        }

        if (id === '34' || id === '35') {
          draft.familia = '0';
        }
        // força diárias com value 7 para planos diferentes do GO
        if (!(id === '34' || id === '35') && draft.diarias === '4') {
          draft.diarias = '7';
        }

        draft.idCard = id;

        break;
      }
      case '@change/FAMILIA': {
        const { id } = action.payload;

        draft.familia = id;

        break;
      }
      case '@change/DIARIAS': {
        const { id } = action.payload;

        draft.diarias = id;

        break;
      }
      case '@change/MENSALIDADE': {
        const { id } = action.payload;
        draft.valorMensalidade = id;
        break;
      }
      case '@cadastro/USER': {
        const { dataUser, numPedido, restricaoSPC } = action.payload;
        draft.infosUser = dataUser;
        draft.numPedido = numPedido;
        draft.restricaoSPC = restricaoSPC;

        break;
      }

      case '@cartao/PAN': {
        const { initialNumber } = action.payload;

        if (draft.binPan.some(starts => starts === initialNumber)) {
          draft.isPan = true;
          draft.adesao = 2;
          draft.PortoModal = true;
          draft.totalMensalidade = draft.valorMensalidade + draft.valorAdesao;
        } else if (draft.idCard === '34' || draft.idCard === '35') {
          draft.isPan = false;
          draft.totalMensalidade = draft.valorMensalidade;
          draft.adesao = 2;
        } else {
          draft.isPan = false;
          draft.adesao = 1;
          draft.totalMensalidade = draft.valorMensalidade;
        }

        break;
      }

      case '@numero/TOKEN': {
        const { token } = action.payload;
        draft.token = token;
        break;
      }

      case '@request/RESPONSE': {
        const { response, pag } = action.payload;
        draft.etapa = pag;
        draft.responseRequest = response;

        break;
      }
      case 'inicio/FORM': {
        const { number } = action.payload;
        if (!draft.isPan) {
          draft.isPan = false;
        }
        draft.etapa = number;
        break;
      }
      case 'valor/ADESAO': {
        const { valor } = action.payload;
        draft.valorAdesao = valor;
        break;
      }
      case '@change/CARTAOCREDITO': {
        draft.erroCartao = false;

        const { id } = action.payload;
        draft.idCartaoCredito = id;
        break;
      }
      case '@validation/CARTAO': {
        draft.erroCartao = true;
        break;
      }
      case '@params/URL': {
        const { id, value } = action.payload;
        draft.urlParams[id] = value;

        break;
      }
      case '@modal/MESSAGE': {
        const { boolean, message } = action.payload;
        draft.messageModal = message;
        draft.modalIsOpen = boolean;
        break;
      }
      case '@modal/PORTOPLUS': {
        const { boolean } = action.payload;
        draft.PortoModal = boolean;
        break;
      }
      case '@codigo/VENDEDOR': {
        const { cod } = action.payload;
        draft.codigoVendedorIS = cod;
        break;
      }
      case '@adesao/DESCONTO': {
        const { valorAdesao } = action.payload;
        draft.valorAdesaoComDesc = valorAdesao;
        break;
      }
      default:
    }
  });
}
