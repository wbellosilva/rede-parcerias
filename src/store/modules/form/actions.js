export function nextPage() {
  return {
    type: '@next/PAGE',
  };
}
export function backPage() {
  return {
    type: '@back/PAGE',
  };
}
export function changeCard(id) {
  return {
    type: '@change/CARD',
    payload: { id },
  };
}

export function changeFamilia(id) {
  return {
    type: '@change/FAMILIA',
    payload: { id },
  };
}
export function changeDiarias(id) {
  return {
    type: '@change/DIARIAS',
    payload: { id },
  };
}
export function changeMensalidade(id) {
  return {
    type: '@change/MENSALIDADE',
    payload: { id },
  };
}
export function cadastro(dataUser, numPedido, restricaoSPC) {
  return {
    type: '@cadastro/USER',
    payload: { dataUser, numPedido, restricaoSPC },
  };
}
export function isPan(initialNumber) {
  return {
    type: '@cartao/PAN',
    payload: { initialNumber },
  };
}
export function addToken(token) {
  return {
    type: '@numero/TOKEN',
    payload: { token },
  };
}
export function responseRequest(response, pag) {
  return {
    type: '@request/RESPONSE',
    payload: { response, pag },
  };
}

export function inicioForm(number) {
  return {
    type: 'inicio/FORM',
    payload: { number },
  };
}
export function setAdesao(valor) {
  return {
    type: 'valor/ADESAO',
    payload: { valor },
  };
}
export function changeCartaoCredito(id) {
  return {
    type: '@change/CARTAOCREDITO',
    payload: { id },
  };
}
export function erroValidationCartao() {
  return {
    type: '@validation/CARTAO',
  };
}
export function urlParams(id, value) {
  return {
    type: '@params/URL',
    payload: { id, value },
  };
}
export function modal(boolean, message) {
  return {
    type: '@modal/MESSAGE',
    payload: { boolean, message },
  };
}
export function modalPorto(boolean) {
  return {
    type: '@modal/PORTOPLUS',
    payload: { boolean },
  };
}
export function codigoVendedorIS(cod) {
  return {
    type: '@codigo/VENDEDOR',
    payload: { cod },
  };
}
export function setAdesaoComDesconto(valorAdesao) {
  return {
    type: '@adesao/DESCONTO',
    payload: { valorAdesao },
  };
}
