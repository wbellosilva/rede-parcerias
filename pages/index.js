import React from 'react';
import Head from 'next/head';
import { Provider } from 'react-redux';
import MotorVendas from '../src/components/MotorVendas/index';

import Header from '../src/components/Header/index';
import ModalError from '../src/components/Modal/index';

import ModalPortoPlus from '../src/components/Modal/isPorto';
import Comentario from '../src/components/Comentario/index';

import Sobre from '../src/components/Sobre/index';
import Video from '../src/components/Video/index';
import Footer from '../src/components/Footer/index';
import Faq from '../src/components/Faq/index';
import store from '../src/store/index';
import LgpdCookies from '../src/components/LgpdCookies';

export default function Home() {
  return (
    <>
      <Head>
        <title>Coob+</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap"
          rel="stylesheet"
        />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"></link>
        <script src="//code.jivosite.com/widget/fOLQToSzLR" async />
        <link rel="shortcut icon" href="Sections/favicon.ico" />
        <script src="//code.jivosite.com/widget/fOLQToSzLR" async />
        <script src="Scripts/google-tag-manager.js" async />
      </Head>

      <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WMRQVSL"
          style={{ 
            height: 0, 
            width: 0, 
            display: 'none', 
            visibility: 'hidden' 
          }}>
        </iframe>
      </noscript>

      <Header />
      <div id="sobre" className="imageBackgroundMeio">
        <Sobre />

        <Provider store={store}>
          <div id="formulario">
            <MotorVendas />
          </div>
          <ModalError />
          <ModalPortoPlus />
        </Provider>
      </div>
      <div className="imageBackgroundFinal">
        <Video />
        <Comentario />
        <div id="comentarios">
          <Faq />
        </div>
      </div>
      <div id="contatos">
        <Footer />
      </div>
      <div id="cookies">
        <LgpdCookies/>
      </div>
    </>
  );
}
