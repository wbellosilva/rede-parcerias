import React from 'react';
import GlobalStyle from '../src/globalStyles/global';
import 'rsuite/dist/styles/rsuite-default.css';

// eslint-disable-next-line react/prop-types
export default function App({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <Component {...pageProps} />
    </>
  );
}
